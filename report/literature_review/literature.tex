\documentclass[11pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps with pdflatex; use eps in DVI mode
								% T\bibliography{literatureBib}eX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}

\title{Literature Review: Neurite Tracing}
\author{Sarun Gulyanon}
%\date{}							% Activate to display a given date or no date
%\linespread{2}
\begin{document}
\maketitle
\section{Introduction}
% why we want to solve this problem
Digital neuron reconstruction provides a very powerful tool for analyzing neurons, which is important for neuroscience studies. It is the crucial step to reverse engineer the brain and study how the brain works, which is one of the grandest challenges for modern science \cite{roy2009}. Neuron reconstruction is the process of extracting neuronal morphology from microscopic imaging data, which is essential for the study of the structure of neuronal cells. The structures of neurons, especially axonal and dendritic arborizations, play a key role in network connectivity. Their quantitative analysis provide insight into the intrinsic and extrinsic factors that influence neuronal development \cite{mei10}. The lack of an automated system hampered the progress of this field since manual reconstruction is labor-intensive, human experts are required for this operation, more data are collected, and imaging throughput increases every year. Therefore, manual tracing becomes infeasible because of the sheer size of the dataset. In the last few decades, there has been much research on the development of new algorithms for automated neuron tracing. Other advantages of automated system are reproducibility, and productivity. The availability of automated systems allows us to process a large scale collection, which will expand the types of questions about neurons that can be addressed \cite{ascoli2011}.

% why we did this
Although many works have been done in this area, there is still a lack of a robust and fully automated system for general-purpose tracing or a tracing system that is sufficiently effective for our dataset of larval Drosophila sensory neurons. Therefore, we introduced a novel tracing method to tackle the varying contrast in our dataset \cite{gulyanon2015}. 

The following sections discuss issues in neuron reconstructions, previous works on automated system, tracing time-lapse images, tracing evaluation, and feature extraction for neuron analysis, thus illustrating the role of neuron reconstruction in a neuron analyzing system.

\section{Literature Review}
\subsection{Neuron Reconstruction Problem}
Automated neuron reconstruction is a challenging problem because of device limitation, noise, and structural complexity and variability \cite{yua09}. Neurites are small compared to the achievable resolution from the image acquisition systems. They are usually just a few voxels wide in the image stack and their boundaries are often blurred owing to microscope point spread function. The input signal to noise ratio (SNR) and contrast can be poor, especially for live neuron imaging. The neuronal structure can be discontinuous or occluded by undesired structure due to imaging system limitations. Another challenge is noise, which includes irrelevant structures, inhomogeneous contrast from the imperfect distribution of the dye, and nonuniform background illumination \cite{mei10}. A third challenge involves the structural complexity of neurites, especially when neurites are intertwined together. It becomes tricky to separate the adjacent neurites in such situations. The neuronal structure also has high variability in its morphology and appearance. This makes it difficult to find a mathematical model that captures this variability, resulting in ambiguous detection.

\subsection{Automated Tracing Technique}
Neuron reconstruction can be seen as two subproblems - image segmentation and tracing. However, some approaches solve both problems simultaneously. 

\paragraph{Automated image segmentation} is a process of separating the neurite from background. It often involves computing tubularity or vesselness because neurites have small width and they are similar to vessel structures. Examples of successful hand-designed models for vessel enhancement are Tensor voting \cite{medioni2000}, Frangi filter \cite{fran98}, and Optimal Oriented Flux (OOF) \cite{law08}. 

Some methods applied edge detection algorithms to solve the problem since neurites are often smooth lines a few voxels wide, similar to edges in the image. Moreover, neurites often have sharp edges or high contrast so the edge detection technique can be effective for neuron segmentation \cite{selinummi2006}.

Recently, many learning-based methods are introduced because of the availability of a dataset like DIADEM \cite{diadem11a} along with ground truth traces. The method described by \cite{sironi2014} defines the vesselness as the distance from the centerline and uses the Support Vector Machine (SVM) to predict the distance. The rapid increase in the number of algorithms for automated tracing in the last decade makes the new approaches feasible. The method in \cite{chen15} exploits existing methods to extract voxels that constitute neurons, which, in turn, are used to train the SVM. Then, the neurite voxels are segregated from background voxels by classifying every voxel using the SVM. 


\paragraph{Automated Tracing methods} can be categorized mainly into global and local methods. Some methods adopt a hybrid of two types of methods. There is a large number of neuron tracing approaches that have been extensively reviewed, e.g. in \cite{mei10} and \cite{wan11a}.

\subparagraph{Global methods} extract skeletons of neurites based on the global signal distribution of the input images \cite{mye11}. One type of these methods exploits the inherent tree structure of neurons. In \cite{gon11}, steerable filter responses \cite{fre91} are used for finding anchor points. A tree is formulated based on the geometric properties of the neurites. The graph is optimized using the K-minimum spanning tree (K-MST) algorithm. The performance of graph-based methods depends heavily on how the graph is formulated and optimized. These types of methods usually formulate edges in the graph using geodesic distance; however, it is easily influenced by noise \cite{fua12}. Therefore, the method in \cite{fua12} formulates the graph with more robust features and adopts a better optimization technique. It formulates the graph using the path classification, which  determines the likelihood of the path being a neurite. It describes edges as Histogram of Gradient Deviation (HGD) descriptors instead of feature vectors derived from geodesic paths. It optimizes using the Quadratic Mixed Integer Program (Q-MIP), which can compute the optimal solution within a small tolerance, rather than a heuristic technique like K-MST. 

Other types of global methods adopt a skeletonization process, which extracts centerlines of the segmented areas. There are two main forms of skeletonization algorithms: grayscale and binarization. An example is the method in \cite{yua09}, which adopts grayscale skeletonization and minimum spanning tree methods. Another example is the Neural Circuit Tracer (NCT) software \cite{cho11}. It starts with filtering the image stack using multi-scale center surround filters (CSF) or Laplacian of Gaussian (LoG). Second, centerlines of the neurites are detected using the voxel coding algorithm \cite{ste09}, which involves binarization. Finally, branches of centerlines are merged together based on a cost function. These types of techniques usually suffer from the binarization errors, which affect the performance of skeletonization. Moreover, they are likely to produce spurs and loops.

\subparagraph{Local methods} usually start from seed points located automatically or manually, and iteratively trace the neurite centerline using local signal features. These types of methods are most effective when the neurites match the model assumptions. The local method in \cite{bas11} uses principal curve recursively to extract the centerline, starting at seed points. Density estimation is computed using kernel density estimation, which acts as a smooth interpolation technique. 

Another type of local method is based on active contour models \cite{kas88, xup98, wan11a, wan11b}, including our method \cite{gulyanon2015}. Snakes are represented by series of points \cite{kas88}, or parametric curves like B-spline \cite{uns00} or Hermite \cite{uhl14} curves. Some of the most popular techniques for solving optimization problem are variational calculus \cite{kas88}, dynamic programming \cite{ami90}, and greedy algorithms \cite{wil92}. Variational calculus operates in continuous space, while dynamic programming and greedy algorithms work in discrete space over pixels/voxels. However, dynamic programming and greedy algorithms do not have problems with numerical instability and these frameworks support hard constraints. Although greedy algorithms are usually inferior to dynamic programming in term of accuracy, they are far more efficient. Most methods, including \cite{wan11a}, adopt the open-curve snake approach and evolve snakes in a sequential manner. In \cite{wan11a}, tubularity is computed using the scalar voting algorithm and snakes are automatically merged in post-processing step. On the other hand, our method \cite{gulyanon2015} evolves multiple snakes simultaneously. This allows us to exploit the geometry properties of nearby snakes to resolve ambiguities.

The advantage of this type of method is that there is a trade-off between information derived from the images and the pre-defined model. Because the biomedical imaging is usually noisy, solely relying on information directly derived from the image usually results in bad performance. Therefore, active contour model framework allows us to incorporate the domain knowledge to improve the results we get from the image.

Some methods adopt a hybrid between global and local methods. The hybrid method in \cite{mye11} uses the model-based tracing, which fits cylinder models to find the neurite branches. Merging branches is formulated as a graph problem based on geodesic distance, which is solved by the MST algorithm.

\subsection{Time-Lapse Tracing}
Normally, neurons are imaged \emph{in vivo} over time. The change of neuron morphology over time is the key to modelling neurons and to understand the processes that occur in a brain.

Many methods reconstruct the structures of each image independently. Then, the evolution of neurons is seen as a dendrite tracking problem \cite{li2011} over time-lapse images. This method \cite{li2011} is based on nonrigid registration for aligning neurons between different time steps and
integer programming for finding the association neurite.

Some research has been conducted to trace neurons in all images simultaneously in order to increase reliability and robustness. Authors in \cite{fua2014} proposed an approach to reconstruct tree structures that evolve over time. This is achieved by formulating the problem as a graph, where nodes represent points on the neuron and edges show the possible topology of neurons. They also added edges of another type that connect nodes in consecutive time steps to enforce the smoothness of change in neurons over time.


\subsection{Tracing Evaluation}
Tracing evaluation normally concentrates on two aspects: accuracy and efficiency. Some of the most popular metrics for evaluating the accuracy of neuron reconstruction are precision and recall, tree edit distance (TED) \cite{zhang1989}, and DIADEM score \cite{diadem11b}. Precision and recall are computed using incorrect traces as false positives (FP), missing traces as false negatives (FN), and correct automatic traces as true positives (TP). The TED metric considers neuronal topologies as trees and compares them by counting the number of edit operations to transform the tree to another tree. On the other hand, DIADEM metric is designed with the goal of reducing human interaction time and reflecting the global topological similarity of the trees. The metric makes the comparison based on matching paths from matching ancestor nodes and path length error.

However, the direct comparison of methods in term of computational efficiency is tricky because these methods were implemented in different programming languages, deployed in different systems, and adopted in different protocols for data representation. In order to overcome this issue, there is an attempt to create a standard in the BigNeuron project \cite{peng2015}.

\subsection{Feature Extractions}
Simply using a collection of image stacks and traces is not effective in studying how neurons are organized and how they differ from one another. Therefore, quantification of neuronal features is required to create a simpler technique for distinguishing neurons. Much research has been previously conducted on quantifying the morphology to determine neuronal subtypes such as L-Measure \cite{sorcioni2008} and Scholl analysis \cite{sholl1953}.
Another example is method \cite{mcgarry2010}, which used morphological and electrophysiological data to determine subtypes of neurons.

We approach this problem differently by partitioning the neuron into compartments like soma, axon, and dendrite. These compartments or the latent part labels are derived using a multi-compartment geometric variant of Active Shape Models \cite{gavriil2012}. Then, we employed latent state Conditional Random Field (CRF) using latent part labels and shape description as features \cite{chang2012, chang2013, chang2014}.




\bibliographystyle{abbrv}
%\bibliography{literatureBib,../NeuronModelBib,../hermitesnakeBib,../bsnakeBib,../crfsnake}
\begin{thebibliography}{10}

\bibitem{ami90}
A.~Amini, T.~Weymouth, and R.~Jain.
\newblock `Using dynamic programming for solving variational problems in vision,'
\newblock {\em IEEE Transactions on Pattern Analysis and Machine Intelligence}, 12(9):855--867, 1990.

\bibitem{bas11}
E.~Bas and D.~Erdogmus.
\newblock `Principal curves as skeletons of tubular objects - locally characterizing the structures of axons,'
\newblock {\em Neuroinformatics}, 9(2-3):181--191, 2011.

\bibitem{uns00}
P.~Brigger, J.~Hoeg, and M.~Unser.
\newblock `B-{S}pline snakes: {A} flexible tool for parametric contour detection,'
\newblock {\em {IEEE} Transactions on Image Processing}, 9(9):1484--1496, 2000.

\bibitem{diadem11a}
K.M.~Brown, G.~Barrionuevo, A.J.~Canty, V.~De~Paola, J.A.~Hirsch, G.S.X.E.~Jefferis, J.~Lu, M.~Snippe, I.~Sugihara, and G.A.~Ascoli.
\newblock `The {DIADEM} data sets: representative light microscopy images of neuronal morphology to advance automation of digital reconstructions,'
\newblock {\em Neuroinformatics}, 9(2-3):143--157, 2011.

\bibitem{chang2012}
X.~Chang, M.D.~Kim, A.~Chiba, and G.~Tsechpenakis.
\newblock `Patterning motor neurons in the drosophila ventral nerve cord using latent state conditional random fields,'
\newblock In {\em IEEE 9th International Symposium on Biomedical Imaging (ISBI), 2012}, pages 864--867, 2012.

\bibitem{chang2013}
X.~Chang, M.D.~Kim, A.~Chiba, and G.~Tsechpenakis.
\newblock `Motor neuron recognition in the drosophila ventral nerve cord,'
\newblock In {\em IEEE 10th International Symposium on Biomedical Imaging (ISBI), 2013}, pages 1488--1491, 2013.

\bibitem{chang2014}
X.~Chang, M.D.~Kim, R.~Stephens, T.~Qu, S.~Gulyanon, A.~Chiba, and G.~Tsechpenakis.
\newblock `Neuron recognition with hidden neural network random fields,'
\newblock In {\em IEEE 11th International Symposium on Biomedical Imaging (ISBI), 2014}, pages 266--269, 2014.

\bibitem{chen15}
H.~Chen, H.~Xiao, T.~Liu, and H.~Peng.
\newblock `Smarttracing: self-learning-based neuron reconstruction,'
\newblock {\em Brain Informatics}, 2(3):135--144, 2015.

\bibitem{cho11}
P.~Chothani, V.~Mehta, and A.~Stepanyants.
\newblock `Automated tracing of neurites from light microscopy stacks of images,'
\newblock {\em Neuroinformatics}, 9(2-3):263--278, 2011.

\bibitem{ascoli2011}
D.E.~Donohue and G.A.~Ascoli.
\newblock `Automated reconstruction of neuronal morphology: An overview,'
\newblock {\em Brain research reviews}, 67(1-2):94--102, 06 2011.

\bibitem{fran98}
A.F.~Frangi, W.J.~Niessen, K.L.~Vincken, and M.A.~Viergever.
\newblock `Multiscale vessel enhancement filtering,'
\newblock In {\em Medical Image Computing and Computer-Assisted Intervention - MICCAI '98}, pages 130--137, 1998.

\bibitem{fre91}
W.T.~Freeman and E.H.~Adelson.
\newblock `The design and use of steerable filters,'
\newblock {\em IEEE Trans. Pattern Anal. Mach. Intell.}, 13(9):891--906, 1991.

\bibitem{diadem11b}
T.A.~Gillette, K.M.~Brown, and G.A.~Ascoli.
\newblock `The diadem metric: Comparing multiple reconstructions of the same neuron,'
\newblock {\em Neuroinformatics}, 9(2-3):233--245, 2011.

\bibitem{fua2014}
P.~Glowacki, M.~Pinheiro, E.~Turetken, R.~Sznitman, D.~Lebrecht, J.~Kybic, A.~Holtmaat, and P.~Fua.
\newblock `Reconstructing evolving tree structures in time lapse sequences,'
\newblock In {\em IEEE Conference on Computer Vision and Pattern Recognition (CVPR), 2014}, pages 3035--3042, 2014.

\bibitem{gulyanon2015}
S.~Gulyanon, N.~Sharifai, S.~Bleykhman, E.~Kelly, M.D.~Kim, A.~Chiba, and G.~Tsechpenakis.
\newblock `Three-dimensional neurite tracing under globally varying contrast,'
\newblock In {\em IEEE 12th International Symposium on Biomedical Imaging (ISBI), 2015}, pages 875--879, 2015.

\bibitem{kas88}
M.~Kass, A.P.~Witkin, and D.~Terzopoulos.
\newblock `Snakes: Active contour models,'
\newblock {\em International Journal of Computer Vision}, 1(4):321--331, 1988.

\bibitem{law08}
M.W.K.~Law and A.C.S.~Chung.
\newblock `Three dimensional curvilinear structure detection using optimally oriented flux,'
\newblock {\em ECCV}, pages 368--382, 2008.

\bibitem{li2011}
Q.~Li, Z.~Deng, Y.~Zhang, X.~Zhou, U.~Nagerl, and S.~Wong.
\newblock `A global spatial similarity optimization scheme to track large numbers of dendritic spines in time-lapse confocal microscopy,'
\newblock {\em  IEEE Transactions on Medical Imaging}, 30(3):632--641, 2011.

\bibitem{mcgarry2010}
L.M.~McGarry, A.M.~Packer, E.~Fino, V.~Nikolenko, T.~Sippy, and R.~Yuste.
\newblock `Quantitative classification of somatostatin-positive neocortical interneurons identifies three interneuron subtypes,'
\newblock {\em Frontiers in Neural Circuits}, 4(12), 2010.

\bibitem{medioni2000}
G.~Medioni, C.-K.~Tang, and M.-S.~Lee.
\newblock `Tensor voting: Theory and applications,'
\newblock {\em Proceedings of RFIA, Paris, France}, 2000.

\bibitem{mei10}
E.~Meijering.
\newblock `Neuron tracing in perspective,'
\newblock {\em Cytometry Part A}, 77A(7):693--704, 2010.

\bibitem{peng2015}
H.~Peng, E.~Meijering, and G.A.~Ascoli.
\newblock `From diadem to bigneuron,'
\newblock {\em Neuroinformatics}, pages 1--2, 2015.

\bibitem{roy2009}
B.~Roysam, W.~Shain, and G.A.~Ascoli.
\newblock `The central role of neuroinformatics in the national academy of engineering's grandest challenge: reverse engineer the brain,'
\newblock {\em Neuroinformatics}, 7(1):1--5, 2009.

\bibitem{sorcioni2008}
R.~Scorcioni, S.~Polavaram, and G.A.~Ascoli.
\newblock `L-measure: a web-accessible tool for the analysis, comparison, and search of digital reconstructions of neuronal morphologies,'
\newblock {\em Nature protocols}, 3(5):866--876, 2008.

\bibitem{selinummi2006}
J.~Selinummi, P.~Ruusuvuori, A.~Lehmussola, H.~Huttunen, O.~Yli-Harja, and R.~Miettinen.
\newblock `Three-dimensional digital image analysis of immunostained neurons in thick tissue sections,'
\newblock In {\em Proc. Conf. IEEE Engineering in Medicine and Biology Society - EMBS '06}, pages 4783--4786, 2006.

\bibitem{sholl1953}
D.A.~Sholl.
\newblock `Dendritic organization in the neurons of the visual and motor cortices of the cat,'
\newblock {\em Journal of Anatomy}, 87:387--406, 1953.

\bibitem{sironi2014}
A.~Sironi, V.~Lepetit, and P.~Fua.
\newblock `Multiscale centerline detection by learning a scale-space distance transform,'
\newblock In {\em IEEE Conference on Computer Vision and Pattern Recognition (CVPR)}, pages 2697--2704, 2014.

\bibitem{gavriil2012}
G.~Tsechpenakis, P.~Mukherjee, M.D.~Kim, and A.~Chiba.
\newblock `Three-dimensional motor neuron morphology estimation in the drosophila ventral nerve cord,'
\newblock {\em IEEE Transactions on Biomedical Engineering}, 59(5):1253--1263, 2012.

\bibitem{fua12}
E.~Turetken, F.~Benmansour, and P.~Fua.
\newblock `Automated reconstruction of tree structures using path classifiers and mixed integer programming,'
\newblock In {\em Conference on Computer Vision and Pattern Recognition}, 2012.

\bibitem{gon11}
E.~T{\"u}retken, G.~Gonz{\'a}lez, C.~Blum, and P.~Fua.
\newblock `Automated reconstruction of dendritic and axonal trees by global optimization with geometric priors,'
\newblock {\em Neuroinformatics}, 9(2-3):279--302, 2011.

\bibitem{uhl14}
V.~Uhlmann, R.~Delgado-Gonzalo, and M.~Unser.
\newblock `Snakes with tangent-based control and energies for bioimage analysis,'
\newblock In {\em Proceedings of the 11th IEEE International Symposium on Biomedical Imaging: From Nano to Macro (ISBI'14)}, 2014.

\bibitem{ste09}
Z.~Vasilkoski and A.~Stepanyants.
\newblock `Detection of the optimal neuron traces in confocal microscopy images,'
\newblock {\em Journal of Neuroscience Methods}, 178(1):197 -- 204, 2009.

\bibitem{wan11b}
Y.~Wang, A.~Narayanaswamy, and B.~Roysam.
\newblock `Novel 4-d open-curve active contour and curve completion approach for automated tree structure extraction,'
\newblock In {\em CVPR}, pages 1105--1112, 2011.

\bibitem{wan11a}
Y.~Wang, A.~Narayanaswamy, C.-L.~Tsai, and B.~Roysam.
\newblock `A broadly applicable 3-d neuron tracing method based on open-curve snake,'
\newblock {\em Neuroinformatics}, 9(2-3):193--217, 2011.

\bibitem{wil92}
D.J.~Williams and M.~Shah.
\newblock `A fast algorithm for active contours and curvature estimation,'
\newblock {\em CVGIP: Image Underst.}, 55(1):14--26, Jan. 1992.

\bibitem{xup98}
C.~Xu and J.L.~Prince.
\newblock `Snakes, shapes, and gradient vector flow,'
\newblock {\em IEEE Transactions on Image Processing}, 7(3):359--369, 1998.

\bibitem{yua09}
X.~Yuan, J.T.~Trachtenberg, S.M.~Potter, and B.~Roysam.
\newblock `Mdl constrained 3-d grayscale skeletonization algorithm for automated extraction of dendrites and spines from fluorescence confocal images,'
\newblock {\em Neuroinformatics}, 7(4):213--232, 2009.

\bibitem{zhang1989}
K.~Zhang and D.~Shasha.
\newblock `Simple fast algorithms for the editing distance between trees and related problems,'
\newblock {\em SIAM journal on computing}, 18(6):1245--1262, 1989.

\bibitem{mye11}
T.~Zhao, J.~Xie, F.~Amat, N.G.~Clack, P.~Ahammad, H.~Peng, F.~Long, and E.W.~Myers.
\newblock `Automated reconstruction of neuronal morphology based on local geometrical and global structural models,'
\newblock {\em Neuroinformatics}, 9(2-3):247--261, 2011.

\end{thebibliography}

\end{document}  