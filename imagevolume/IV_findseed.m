function [Vseed, seedp] = IV_findseed( Iout, mask )
%FINDSEEDPOINT find seed volume and seed points in XYZ-coordinate from
%preprocess volume using local maxima of OOF response. Hessian based
%technique like Frangi give false positive because of clutter neurite.

REPORT = false;
rect = [30, 30, 200, 200];

%% Find candidate points
[fx,fy,~] = AM_gradient(Iout);
if ndims(mask) == 3
    seedsx = fx(:,1:end-1,:)>0 & fx(:,2:end,:)<0 & mask(:,1:end-1,:);
    seedsy = fy(1:end-1,:,:)>0 & fy(2:end,:,:)<0 & mask(1:end-1,:,:);
else
    OOF_thr = mask;
    seedsx = fx(:,1:end-1,:)>0 & fx(:,2:end,:)<0 & Iout(:,1:end-1,:)>OOF_thr;
    seedsy = fy(1:end-1,:,:)>0 & fy(2:end,:,:)<0 & Iout(1:end-1,:,:)>OOF_thr;
end
seeds = seedsx(1:end-1,:,:) | seedsy(:,1:end-1,:);
seeds = padarray(seeds,[1 1],'symmetric','post');

if REPORT
    tmp = imcrop(max(seeds, [], 3), rect);
    figure(8), imshow(tmp, [], 'Border','tight');
end

%% keep local maxima of OOF response as seed point
[x,y,z] = meshgrid(-3:3,-3:3,-3:3);
x = x(:); y = y(:); z = z(:);
s = size(Iout);
max_size = ones(numel(x),1) * s([2,1,3]);
p_ind = find(seeds > 0);
[py, px, pz] = ind2sub(s, p_ind);
keep = false(1,length(p_ind));
parfor i = 1:length(p_ind)
    % check each candidate against other candidates in 27-neighborhood
    nb = [px(i)+x, py(i)+y, pz(i)+z];
    cond = all(nb>=1,2) & all(nb<=max_size,2);
    idx = sub2ind(s, py(i)+y(cond), px(i)+x(cond), pz(i)+z(cond));
    idx = idx(seeds(idx));      % max among local seed candidates
    [~,maxpos] = max(Iout(idx));
    % keep the local maximum of OOF response
    if p_ind(i) == idx(maxpos)
        keep(i) = true;
    end
end

% get XYZ-coordinate of seed point and seed volume
Vseed = false(s);
Vseed(p_ind(keep)) = true;
seedp = [px(keep), py(keep), pz(keep)];

if REPORT
    tmp = imcrop(max(Vseed, [], 3), rect);
    figure(9), imshow(tmp, [], 'Border','tight');
%     keyboard;
end
end