function data = trace_main2( V, Iout, scale, mask, varargin )
%TRACE_MAIN2 trace neuron using snake population


% parameters
defaultoptions = struct('oof_thr', 4, 'zeta', 0.5, 'gvfiter', 3, 'length_thr', 10);
options = get_options(defaultoptions, varargin);

DEBUG = 1;
REPORT = 0;
PRESENTATION = 0;
SAVE_AVI = 0;

% OOF_thr = 4;        % threshold of OOF response
% OOF_thr = 3;        % 73113_2
ZRATIO = 4;         % z-ratio against xy of pixel size
sigmas = 2;         % var of guassian smooth for find neurite direction

max_iter = 500;     % max number of HCF iteration
BLOCKSIZE = 20;     % size of snake grid cell, for optimize compute model

ALPHA = 0.5;        % snake parameters
BETA = 0.1;
GAMMA = 2.0;
ZETA = options.zeta;
% ZETA = 0.5;         % region, 72513_h0
% ZETA = 0.3;         % 72413_h0, 73113_1
MAXV = -log(1/2);   % max value of Eimg
MAXCON = 3;         % no. of non-decrease Esnk allowed before converge


if PRESENTATION,
    V = V(175:425,275:575,:);
    Vpre = Vpre(175:425,275:575,:);
    scale = scale(175:425,275:575,:);
    Iout = Iout(175:425,275:575,:);
end

% Get neurite direction
disp('Get Neurite Direction Started...')
[Vx, Vy, Vz] = IV_eig(Iout, sigmas);

% find seed points (need to improve by remove seed like in Fua paper)
disp('Find Seed Point Started...')
[Vseed, seedp] = IV_findseed( Iout, mask ); 
if DEBUG
    figure(2); imshow3Doverlay(V, Vseed);
end

%% convert OOF response to energy function
% also give penalty when intensity is lower than threshold
E = -log((1+Iout)./2);

%% initialize snakes
disp('Initialize Snakes Started...')
snakes = SN_initsnake(E, seedp, Vx, Vy, Vz, ALPHA, BETA, GAMMA, ZETA);

%% local highest confidence first (Local HCF) inits
n = length(snakes);
collide = spalloc(n,n,4);
[stability, graph, snkgrid] = HCF_initstability(snakes, E, BLOCKSIZE);
[stability, graph, snkgrid] = HCF_updatestability(...
        snakes, stability, graph, snkgrid, true(1,n), BLOCKSIZE, MAXCON, ZRATIO);

%% debug
if SAVE_AVI,
    close all
end

if REPORT
%     figure(3), SN_show(V, snakes, [], [], [50, 250, 350, 650] );
    figure(3), SN_show(V, snakes, [], [], [500, 350, 750, 680] );
    print(3, '-dpng', 'tmp_0.png', '-r300')
    keyboard;
end
if SAVE_AVI,
    figure(3); show_snk_ppt( V, snakes, collide, scale, false(1,n), MAXCON);
    set(gcf, 'Position', [150 500 1280 800]);
    outputVideo = VideoWriter('demo.avi');
    open(outputVideo);
    writeVideo(outputVideo, getframe(gcf));
end

%% local highest confidence first (Local HCF) starts
disp('HCF Started...')
for iter = 1:max_iter
    if mod(iter, 10) == 0,
        disp(['Iter: ' num2str(iter) ', Total snake energy = ', num2str(sum([snakes(:).E]))]);
    end
    
    % pre-compute neighborhood variables to allow parallel
    nbhd_rank = zeros(1,n);
    nbhd_snakes = cell(1,n);
    for i = 1:n
        idx = find(graph(i,:));
        [~, minpos] = min(stability(idx));
        nbhd_rank(i) = idx(minpos);
        nbhd_snakes{i} = snakes(idx(idx~=i));
    end
    
    % deform every snake
    change = false(1,n);
    for i = 1:n
        % evolve snake that has non-positive stability, lower stability
        % than its neighbour. If stability is equal, index (or rank) of
        % snake must be lower.
        if stability(i) < 0 && i == nbhd_rank(i)
            % evolve snake using dynamic programming
            % add interaction potential (include snk length in formula)
            % site merge when snk merge (so no interaction potential, 
            % only snk energy)
            [snakes(i), collide(i,:)] = SN_deform_att(...
                    snakes(i), nbhd_snakes{i}, E, collide(i,:), scale, ...
                    ALPHA, BETA, GAMMA, ZETA, MAXV, MAXCON, ZRATIO);
            change(i) = true;
        else
            snakes(i).minE = snakes(i).E;
        end
    end

    % check if no changes, population of snakes converge
    any_change = any(change);
    if ~any_change
        break
    end
    
    % compute snake components for attraction energy
    lenCC = -ones(1,n);
    comp = components(collide+collide');
    for i = 1:n
        if lenCC(comp(i)) < 0
            lenCC(comp(i)) = sum(arrayfun(@(x)size(x.vert,1), snakes(comp==comp(i))));
        end
        snakes(i).CClen = lenCC(comp(i));
        snakes(i).CCno = comp(i);
    end
    
    %** may need to mex this to speed up**
    [stability, graph, snkgrid] = HCF_updatestability(...
            snakes, stability, graph, snkgrid, change, BLOCKSIZE, MAXCON, ZRATIO);
    % display result step-by-step
    if REPORT
%         figure(4), SN_show(V, snakes, collide, [], [50, 250, 350, 650] );
        figure(4), SN_show(V, snakes, collide, [], [500, 350, 750, 680] );
        print(4, '-dpng', ['tmp_' num2str(iter) '.png'], '-r300')
    end
    if SAVE_AVI,
%         figure(4), SN_show(V, snakes, collide);
        figure(4); show_snk_ppt( V, snakes, collide, [], change, MAXCON);
        set(gcf, 'Position', [150 500 1280 800]);
        writeVideo(outputVideo, getframe(gcf));
    end
%     pause
end
disp('Done...')

%%
data = SN_postprocess( Iout, snakes, collide, scale, ...
        'maxiter', options.gvfiter, 'length_thr', options.length_thr);

if SAVE_AVI,
    figure(4); show_snk_ppt( V, snakes, collide, [], change, MAXCON);
    subplot(1,2,1); EV_plot( V, data, '2D' )
    set(gcf, 'Position', [150 500 1280 800]);
    writeVideo(outputVideo, getframe(gcf));
    close(outputVideo);
end

end

