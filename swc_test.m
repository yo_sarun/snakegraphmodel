addpath(genpath('matlab_bgl'))
addpath('metric')

% filename = 'swc/t72413_h05.swc';
% filename = 'swc/t72513_h1.swc';
% filename = 'swc/t73113_1_h05.swc';
% filename = 'swc/t73113_2_h115.swc';
data = EV_readSWC(filename);
E = data(:,[1,7]);
n = size(E,1);
if sum(sum(E==-1)) ~= 1
    error('ERROR: Multiple tress in one SWC file')
end
E(any(E==-1,2),:) = [];
G = sparse(E(:,1), E(:,2), 1, n, n);
if unique(components(G+G')) ~= 1
    error('ERROR: Multiple components in SWC file')
end
disp([filename ': GOOD SWC'])