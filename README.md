# README #

This is the implementation of CRF Formulation Of Active Contour Population For Efficient Three Dimensional Neurite Tracing

### What is this repository for? ###

* Tracing neurite with varying contrast background using multiple active contour models or snakes

### How do I get set up? ###

* Work with MATLAB 2014a
* run script.m
* main tracing function is trace_main.m
```matlab
% Usage
%
% data = trace_main(imgdir, savefile, preprocfunc, varargin)
%
% This function trace the single neuron in the image stack with varying
% contrast in the background using multiple active contour models or snakes
%
% Citation :
% S. Gulyanon, N. Sharifai, M.D. Kim, A. Chiba, and G. Tsechpenakis, "CRF 
% Formulation Of Active Contour Population For Efficient Three Dimensional 
% Neurite Tracing," Int'l Symposium on Biomedical Imaging: from Nano to 
% Macro (ISBI), Prague, Czech Republic, 2016.
%
% Inputs:
%   imgdir        diretory containing slices of image stacks
%   savefile      file location for saving the trace in SWC format
%                 Set to zero if length of slope doesn't matter.
%   preprocfunc   A preprocessing function that takes image stack V and
%                 aspect ratio in Z-axis ZRATIO. It needs to return
%                 preprocessed image stack (Vrm), vesselness measurement
%                 (Iout), thinkness of neuron in every voxel (scale).
%   varargin      miscellaneous parameters.
%       'oof_thr'           OOF threshold. Default 4
%       'zeta'              weight for attraction energy. Default 0.5 
%       'e_att'             Using attraction energy. Default true
%       'gau_val'           Gaussian smoothness size and sigma. Default 10
%       'intensity_thr'     Intensity threshold. Default 50
%       'region_size'       Minimum binary component size. Default 100
%       'gvfiter'           Number of iteration for GVF. Default 3
%       'length_thr'        Neurite minimum length. Default 5
%
% Outputs:
%   data          Tracing results in SWC format
%
```
### Citation ###
S. Gulyanon, N. Sharifai, M.D. Kim, A. Chiba, and G. Tsechpenakis, "CRF Formulation Of Active Contour Population For Efficient Three Dimensional Neurite Tracing," Int'l Symposium on Biomedical Imaging: from Nano to Macro (ISBI), Prague, Czech Republic, 2016.