function data = EV_readSWC( f )
%EV_READSWC converts file f to a n-by-7 matrix data from neuron file (.swc file)
%Input = f - filenmae of neuron (.swc file)
%Output = data - n-by-7 matrix data from neuron file (.swc file)

data = [];
% read swc
fid=fopen(f, 'r'); 
if fid == -1 
    error('File could not be opened, check name or path.')
end
% skip comments
tline = '#';
while tline(1) == '#'
    tline = strtrim(fgetl(fid));
end
% get neurite points
while ischar(tline) 
    vnum = sscanf(tline, '%d %d %f %f %f %f %d');
    data = [data; vnum'];
    tline = fgetl(fid);
end
fclose(fid);
end

