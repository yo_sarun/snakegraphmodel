function [ precision, recall, MES, m, e ] = EV_pr( goldswc, testswc, RTHR )
%EV_PR find precision/recall over length of traces
%** may need to check there is only one trace component in goldswc file
FP = 0;     % test points in background
FN = 0;     % gold points with no test points inside both partially/fully
TP = 0;

DEBUG = 0;
if nargin < 3
    RTHR = 4;
end
if ~isempty(RTHR) && RTHR <= 0,
    RTHR = [];
end

%% remove test points outside gold standard, and count FP
% make graph for testswc
nT = size(testswc,1);
edge = testswc(testswc(:,7) ~= -1,[1, 7]);
GT = sparse(edge(:,1), edge(:,2), 1, nT, nT);
GT = GT + GT' > 0;

% get direction (unit vector) of gold trace edge
prevG = goldswc(:,7);
prevG(prevG == -1) = goldswc(prevG == -1,1);
dG = goldswc(prevG,3:5) - goldswc(:,3:5);
dGlen = sqrt(sum(dG.^2,2));
udG = bsxfun(@rdivide, dG, dGlen);

% make graph for goldswc
nG = size(goldswc,1);
edge = goldswc(goldswc(:,7) ~= -1,[1, 7]);
GG = sparse(edge(:,1), edge(:,2), 1, nG, nG);
GG = GG + GG' > 0;

% remove test points outside gold points/edges
overlap = zeros(1,nT);
outside = false(1,nT);
parfor i = 1:nT
    % check test point inside any sphere at gold points
    v = bsxfun(@minus, testswc(i,3:5), goldswc(:,3:5));
    vlen = sqrt(sum(v.^2,2));
    [vmin, vpos] = min(vlen);
    if isempty(RTHR), 
        radthr = goldswc(vpos, 6);
    else
        radthr = RTHR;
    end
    if vmin <= radthr
        overlap(i) = vpos;
        continue
    end
    
    % check test point inside any trucated cone at gold edges
    vcos = sum(v.*udG,2);
    % check above cone base and below cone top
    idx = find(0 <= vcos & vcos <= dGlen);
    % check inside slice circle
    vsin = sqrt(vlen(idx).^2-vcos(idx).^2);
    [vmin, pos] = min(vsin);
    vpos = idx(pos);
    alpha = vcos(vpos)/dGlen(vpos);
    radii = (1-alpha)*goldswc(vpos,6) + alpha*goldswc(prevG(vpos),6);
    if isempty(RTHR), 
        radthr = radii;
    end
    if vmin <= radthr,
        if alpha < .5
            overlap(i) = vpos;
        else
            overlap(i) = prevG(vpos);
        end
    else
        outside(i) = true;
    end
end

% remove points outside, count as FP
visited = false(nT,nT);
pFP = zeros(nT,1);
for i = find(outside)
    for j = find(GT(i,:))
        if visited(i,j), continue, end
        visited(i,j) = true;
        visited(j,i) = true;
        ds = testswc(i,3:5) - testswc(j,3:5);
        FP = FP + sum(sqrt(sum(ds.^2,2)));
        pFP(i) = pFP(i) + sum(sqrt(sum(ds.^2,2)));
    end
end
% just remove edges
GT(outside,:) = 0;
GT(:,outside) = 0;

%debug
if DEBUG,
    figure(1),
    smp = 0:0.2:2*pi;
    plot(goldswc(1,3), goldswc(1,4));
    hold on;
    for k = 1:nG
        rad = goldswc(k,6);
        px = goldswc(k,3) + rad*cos(smp);
        py = goldswc(k,4) + rad*sin(smp);
        plot3(px, py, goldswc(k,5)*ones(size(px)), 'b');

        if goldswc(k,7) == -1, continue, end
        idx = goldswc(k, [1,7]);
        plot3(goldswc(idx,3), goldswc(idx,4), goldswc(idx,5), 'b');
    end

    scatter3(testswc(~outside,3), testswc(~outside,4), testswc(~outside,5), 'k')
    scatter3(testswc(outside,3), testswc(outside,4), testswc(outside,5), 'r')
    hold off;
end

% remove test edges outside gold, 
% parent points of goldswc must be in increasing order.
[n1,n2] = find(GT);
rm = false(1,length(n1));
for i = 1:length(n1)
    if nnz(GT(n1(i),n2(i))) < 1, continue, end
    % get node on goldswc that overlap with test edge
    [~, ~, pred] = bfs(GG, overlap(n1(i)), struct('target', overlap(n2(i))));
%     [~, ~, pred] = bfs(GG, overlap(n1(i)), struct('target', overlap(n2(i))));
    path = path_from_pred(pred, overlap(n2(i)));
    
    % check test edge are inside gold points (not endpoints, already knew)
    path = path(2:end-1);
     
    u = bsxfun(@minus, testswc(n2(i),3:5), testswc(n1(i),3:5));
    ulen = sqrt(sum(u.^2,2));
        
    % project gold node on test edge, check test edge is inside nodes
    du = bsxfun(@rdivide, u, ulen);
    v = bsxfun(@minus, goldswc(path,3:5), testswc(n1(i),3:5));
    vlen = sqrt(sum(v.^2,2));
    vcos = v*du';
    vsin = sqrt(vlen.^2-vcos.^2);
    radii = goldswc(path,6);
    
    if isempty(RTHR), 
        rad_thr = radii;
    else
        rad_thr = RTHR;
    end
    % remove edge
    rm(i) = ~all(vsin<=rad_thr);
    if rm(i)
        FP = FP + ulen;
        pFP(n1(i)) = pFP(n1(i)) + ulen;
        GT(n1(i),n2(i)) = 0;
        GT(n2(i),n1(i)) = 0;
    end
end

%debug
if DEBUG,
    figure(2),
    smp = 0:0.2:2*pi;
    plot(goldswc(1,3), goldswc(1,4));
    hold on;
    for k = 1:nG
        rad = goldswc(k,6);
        px = goldswc(k,3) + rad*cos(smp);
        py = goldswc(k,4) + rad*sin(smp);
        plot3(px, py, goldswc(k,5)*ones(size(px)), 'b');

        if goldswc(k,7) == -1, continue, end
        idx = goldswc(k, [1,7]);
        plot3(goldswc(idx,3), goldswc(idx,4), goldswc(idx,5), 'b');
    end
    for k = 1:length(n1)
        idx = [n1(k), n2(k)];
        if rm(k),
            plot3(testswc(idx,3), testswc(idx,4), testswc(idx,5), 'r');
        else
            plot3(testswc(idx,3), testswc(idx,4), testswc(idx,5), 'k');
        end
    end
    hold off;
    keyboard;
end

%% Go through remaining components to find missing trace
[n1,n2] = find(GT);
degT = histc([n1;n2]', 1:nT) / 2;
visited = false(1,nT);
trace = logical(sparse(nG,nG));
miss = sparse(nG,nG);
part = sparse(nG,nG);
while any(~visited)
    root = find(~visited,1);
    % ignore point outside of trace
    if outside(root),
        visited(root) = true;
        continue
    end
    [dT, ~, predT] = bfs(GT, root);
    visited(dT>=0) = true;
    % if there is only one point in this component, skip it.
    if sum(dT>=0) <= 1, continue, end
    idx = find(dT>=0);
    % find corresponding gold points of test points
    [~, ~, predG] = bfs(GG, overlap(root));
    for i = idx(degT(idx)==1)'
        % find test trace path and its corresponding gold trace path
        pathT = path_from_pred(predT, i);
        pathG = path_from_pred(predG, overlap(i));
        % if test path is inside a section of gold trace, it is good
        if length(pathG)<=1, continue, end
        
        % check the root
        if degT(root) == 1 && miss(pathG(1),pathG(2))==0 && trace(pathG(1),pathG(2))==0
            % test path is shorter than gold path
            r = testswc(pathT(1),3:5) - goldswc(pathG(1),3:5);
            rlen = sqrt(sum(r.^2,2));
            if isempty(RTHR), 
                rad_thr = goldswc(pathG(1),6);
            else
                rad_thr = RTHR;
            end
            if rlen < rad_thr%goldswc(pathG(1),6)
                % test path inside first point of gold path, good trace
                trace(pathG(1),pathG(2)) = true;
                trace(pathG(2),pathG(1)) = true;
            else
                % bad trace and get penalty by length between ground truth
                % and test trace point. Count length by projection of test
                % on ground truth capped by length of gold edge.
                d = testswc(pathT(1),3:5) - goldswc(pathG(2),3:5);
                u = goldswc(pathG(1),3:5) - goldswc(pathG(2),3:5);
                ulen = sqrt(sum(u.^2,2));
                du_proj = min(d*(u/ulen)', ulen);
                penal = max(rlen, 1);
                miss(pathG(2),pathG(1)) = penal;
                miss(pathG(1),pathG(2)) = penal;
                part(pathG(2),pathG(1)) = du_proj;
                part(pathG(1),pathG(2)) = du_proj;
            end
        end
        
        % check the end of the path
        if miss(pathG(end-1),pathG(end))==0 && trace(pathG(end-1),pathG(end))==0
            % test path is shorter than gold path
            r = testswc(pathT(end),3:5) - goldswc(pathG(end),3:5);
            rlen = sqrt(sum(r.^2,2));
            if isempty(RTHR), 
                rad_thr = goldswc(pathG(end),6);
            else
                rad_thr = RTHR;
            end
            if rlen < rad_thr%goldswc(pathG(end),6)
                % test path is inside last point in gold path, good trace
                trace(pathG(end-1),pathG(end)) = true;
                trace(pathG(end),pathG(end-1)) = true;
            else
                % bad trace and get penalty by length between ground truth
                % and test trace point. Count length by projection of test
                % on ground truth capped by length of gold edge.
                d = testswc(pathT(1),3:5) - goldswc(pathG(2),3:5);
                u = goldswc(pathG(1),3:5) - goldswc(pathG(2),3:5);
                ulen = sqrt(sum(u.^2,2));
                du_proj = min(d*(u/ulen)', ulen);
                penal = max(rlen, 1);
                if miss(pathG(end-1),pathG(end)) ~= 0, 
                    keyboard;
                end
                miss(pathG(end-1),pathG(end)) = penal;
                miss(pathG(end),pathG(end-1)) = penal;
                part(pathG(end-1),pathG(end)) = du_proj;
                part(pathG(end),pathG(end-1)) = du_proj;
            end
        end
        
        % mark non-endpoints of gold edges as traced 
        trace(sub2ind(size(trace), pathG(2:end-2), pathG(3:end-1))) = true;
        trace(sub2ind(size(trace), pathG(3:end-1), pathG(2:end-2))) = true;
    end
end

% count FN and TP
[n1,n2] = find(tril(GG));
d = goldswc(n1,3:5) - goldswc(n2,3:5);
dlen = sqrt(sum(d.^2,2));
pTP = zeros(nG,1);
pFN = zeros(nG,1);
for i = 1:length(n1)
    if trace(n1(i),n2(i))
        % edge is traced properly
        TP = TP + dlen(i);
        pTP(n1(i)) = pTP(n1(i)) + dlen(i);
    elseif miss(n1(i),n2(i)) > 0
        % partially missed edge
        FN = FN + miss(n1(i),n2(i));
        pFN(n1(i)) = pFN(n1(i)) + miss(n1(i),n2(i));
        TP = TP + part(n1(i),n2(i));
        pTP(n1(i)) = pTP(n1(i)) + part(n1(i),n2(i));
    else
        % fully missed edges
        FN = FN + dlen(i);
        pFN(n1(i)) = pFN(n1(i)) + dlen(i);
    end
end

%% calculate precision, recall, miss-extra-score. Clean up miss, extra.
precision = TP/(TP+FP);
recall = TP/(TP+FN);
S_G = sum(dlen);
MES = (S_G-FN) / (S_G+FP);

m = [goldswc(:,3:5), pFN];
m(pFN<=0,:) = [];
e = [testswc(:,3:5), pFP];
e(pFP<=0,:) = [];

end

