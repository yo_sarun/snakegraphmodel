function cout = EV_get_report( goldfile, testfile, varargin )
%EV_GET_REPORT show score and other measurements of our trace result 
%against given ground truth trace.

options = get_options(struct('neuron_type', 5, 'pr_thr', [], 'v', []), varargin);

if ~ischar(testfile),
    % for multiple files, show in table.
    cout = zeros(length(testfile), 6);
    goldswc = EV_readSWC(goldfile);
    fprintf('=================================   %28s  =========================================\n', goldfile)
    disp(' trace file             |DIADEM |   P   |   R   |  F1   |  MES  |   MAE   ||   miss   |  extra   |   sum')
    disp('-----------------------------------------------------------------------------------------------------------')    
    for n = 1:length(testfile)
        st = find(testfile{n}=='/' | testfile{n}=='\', 1, 'last') + 1;
        if isempty(st),
            st = 1;
        end
        name = testfile{n}(st:end);

        if exist(testfile{n}, 'file') == 0
            fprintf('%23s |  not found\n', name);
        else
            [score, m, e] = EV_diadem(goldfile, testfile{n}, options.neuron_type);
            testswc = EV_readSWC(testfile{n});
            [p, r, mes, ~, ~] = EV_pr(goldswc, testswc, 4);
            mae = EV_MAE(goldswc, testswc);
            f1 = max(0, 2*p*r/(p+r));
            cout(n,:) = [score, p, r, f1, mes, mae];
            if length(name) > 23, name = name(1:23); end
            fprintf('%23s | %.3f | %.3f | %.3f | %.3f | %.3f | %7.3f |', name, score, p, r, f1, mes, mae);
            fprintf('| %8.2f | %8.2f | %8.2f \n', sum(m(:,end)), sum(e(:,end)), sum(m(:,end))+sum(e(:,end)));
        end
    end
    fprintf('\n')
else
    % for single file, show in text.
    [score, m, e] = EV_diadem(goldfile, testfile, options.neuron_type);
    testswc = EV_readSWC(testfile);
    goldswc = EV_readSWC(goldfile);
    [p, r, mes, ~, ~] = EV_pr(goldswc, testswc, options.pr_thr);
    mae = EV_MAE(goldswc, testswc);
    f1 = max(0, 2*p*r/(p+r));
    cout = [sprintf('    D: %.3f, [P: %.3f R: %.3f], F1: %.3f, (MES: %.3f, MAE: %7.3f)\n', score, p, r, f1, mes, mae), ...
            sprintf('    MISS: %8.2f, EXTRA: %8.2f, SUM: %8.2f \n', sum(m(:,end)), sum(e(:,end)), sum(m(:,end))+sum(e(:,end)))];
    if ~isempty(options.v),
        EV_plot( options.v, '2D', [], goldswc, testswc);
        hold on
        scatter(e(:,1)+1, e(:,2)+1, (1+e(:,4))*50, 'MarkerEdgeColor', 'y')
        scatter(m(:,1)+1, m(:,2)+1, (1+m(:,4))*50, 'MarkerEdgeColor', 'g')
        hold off
    end
end
end
