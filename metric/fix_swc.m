function [data, h] = fix_swc( data, factor, V )
%FIX_SWC find only one neuron from the given .swc and save into file
%	  Input
%	  loadfile		load filename
%	  savefile		save filename
%	  V				3D image stack
%	  factor		for resizing trace when downsample the image stack
%

if nargin < 2
    factor = 1;
end

%% Find Spanning Tree
N = size(data,1);
E = [data(:,1) data(:,7)];
E = E(all(E>0,2),:);
DG = sparse(E(:,1),E(:,2), ones(size(E,1),1), N, N);
UG = DG + DG';
ST = kruskal_mst(UG);

[ci,sizes] = components(ST);
[~,comp] = max(sizes);

%% Rearrange points not to violate 
[order,pred] = bfs_traverse(ST,find(ci==comp,1));

data = data(order,:);
data(:,7) = pred(order);
data(:,3:4) = data(:,3:4) .* factor;
newIdx = zeros(size(order));
for i = 1:size(order,2)
    newIdx(order(i)) = i;
end
data(:,1) = newIdx(data(:,1));
data(1,7) = -1;
data(2:end,7) = newIdx(data(2:end,7));

%% show image
if nargin == 3
    h = figure;
    imshow(max(V,[],3),[]);
    hold on
    for i = 1:size(data,1)
        if data(i,7) > 0
            idx = [i, data(i,7)];
            plot(data(idx,3), data(idx,4), 'c', 'Linewidth', 2)
        end
    end
    hold off
end
end

