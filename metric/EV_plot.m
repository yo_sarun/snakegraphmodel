function EV_plot( Vpre, data, mode, rect )
%EV_PLOT plot swc data. Two modes available: 2D or 3D

if nargin < 4
    rect = [1, 1, size(Vpre,1), size(Vpre,2)];
end
maxsize = [rect(4)-rect(2)+1, rect(3)-rect(1)+1];
tmp = bsxfun(@minus, data(:,3:4), [rect(2)-1, rect(1)-1]);

cc = zeros(1,size(data,1));
cur = 1;

switch mode
    case '2D'
        colmap = autumn(sum(data(:,7)==-1));
        
        % draw image
        %imagesc(mean(Vpre,3));
%         imshow(mean(Vpre(rect(1):rect(3), rect(2):rect(4), :), 3), [], 'Border', 'tight');
%         imshow(mean(Vpre(rect(1):rect(3), rect(2):rect(4), :), 3), [], 'Border', 'tight');
        imshow(max(Vpre,[],3),[]);
        %imagesc(mean(Vpre, 3)); colormap gray;
        %colormap gray;
        %axis off
%         set(gca,'position',[0.05 0.05 .45 .9])

        % draw line
        hold on;
        for i = 1:size(data,1)
            prev = data(i,7);
            if prev == -1
                cc(i) = cur;
                cur = cur + 1;
            else
                cc(i) = cc(prev);
                if all(tmp(i,:) >= 1) && all(tmp(i,:) < maxsize)
                    plot([tmp(i,1), tmp(prev,1)], [tmp(i,2), tmp(prev,2)], ...
                            'LineWidth', 2, 'Color', colmap(cc(i),:));
%                     plot([data(i,3), data(prev,3)], [data(i,4), data(prev,4)], ...
%                             'LineWidth', 2, 'Color', colmap(cc(i),:));
                end
            end
        end
        hold off;
        
    case '3D'
        colmap = lines(sum(data(:,7)==-1));
        
        % draw line
        plot3(data(1,3), data(1,4), data(1,5));
        hold on
        for i = 1:size(data,1)
            prev = data(i,7);
            if prev == -1
                cc(i) = cur;
                cur = cur + 1;
            else
                cc(i) = cc(prev);
                idx = [i, prev];
                plot3(data(idx,3), data(idx,4), data(idx,5), 'Color', colmap(cc(i),:));
            end
        end
        hold off
end
end

