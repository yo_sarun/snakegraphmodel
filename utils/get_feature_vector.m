function X = get_feature_vector(data, V, varargin)
%GET_FEATURE_VECTOR get feature vector of neuron
%   Input
%   data        n-by-7 matrix representing the neuron
%   V           3D image stack of neuron
%   varargin    utility options
%
%   Output
%   X           Feature vector of neuron in 1D struct array, each attribute
%               must be a scalar or 1-by-m array (or row vector) or 2D array
%

X = [];

% process input
options = struct('soma', mean(data(:,3:4)), 'issoma', 1,...
        'distance_thr', 3, 'ANG', 8, 'DIS', 4);
optionNames = fieldnames(options);

for pair = reshape(varargin,2,[]) % pair is {propName;propValue}
   inpName = lower(pair{1}); % make case insensitive
   if any(strcmp(inpName,optionNames))
      options.(inpName) = pair{2};
   else
      error('%s is not a recognized parameter name',inpName)
   end
end

if options.issoma == 1
    thr = 30;
else
    thr = 0;
end

% features by Emmanuel
try
% tic;
X.branches = branches(data);
X.branches2 = branches2(data, options.distance_thr);
% X.eroding = eroding(V);
% X.diameter = diametersoma(V);
% X.heightandwidth = heightandwidth(data);
[X.distancebranchMean, X.distancebranchSTD] = distancebranch(data);
[X.branchangleMean,~,X.branchangleSTD] = branchangle2(data);
X.shapehistogrambr = shapehistogrambr(data, options.soma, options.ANG, options.DIS);
X.shapehistogramang = shapehistogramang2(data, options.soma, options.ANG, options.DIS);
X.orientation = orientation(data, options.ANG);
X.curvaturehistogram = curvaturehistogram(data, options.soma, options.ANG, options.DIS);
% toc;

% features by Eves
% tic;
X.leafnodefunction = leafnodefunction(data, options.issoma);
X.longestdendrite = longestdendrite(data);
X.shortestdendrite = shortestdendrite(data, options.soma, thr);
[X.dendritelengthMean, X.dendritelengthSTD] = dendritelengthmean(data, options.soma, thr);
X.dendritelengthsum = dendritelengthsum(data, options.soma, thr);
X.dendritedensity = dendritedensity(data, options.soma);
X.farthestnodes = farthestnodes(data);
X.branchaverage = branchaverage(data, options.issoma);
X.spinedensity = spinedensity(data);
X.spinedensity2 = spinedensity2(data);
% X.somaorientation2 = somaorientation2(V);
% X.somatocenter = somatocenter(data,V);
% X.soma_position = soma_position(data,V);
X.shapehistogram2 = shapehistogram2(data, options.soma, options.DIS, options.ANG);
X.neuronsymmetry = neuronsymmetry(data, options.soma);
X.convhullarea = convhullarea(data);
[~, X.trianglematrix] = trianglematrix(data);
% toc;

% features by Samantha
% tic;
X.lengthofneuron = lengthofneuron(data);
X.circumference = circumference(data);
% X.somatic_dendrites = somatic_dendrites(soma_estimation(V, 200), data);
[X.interpolationhistogram, X.interpolationhistogramMean, X.interpolationhistogramSTD] ...
        = interpolationhistogram(data, options.soma, options.DIS, options.ANG);
[~, X.tortuousityMean, X.tortuousitySTD] = tortuousity(data, options.soma, thr);
[X.no_overlap_tortuousityMean, X.no_overlap_tortuousitySTD] = no_overlap_tortuousity(...
        data, options.soma, thr);
[X.branch_point_averageMean, X.branch_point_averageSTD] = branch_point_average(...
        data, options.soma, thr);
[~, X.curvature_path] = curvature_path (data, options.soma, thr);
% toc;

% features by Yo
X.shapehistogramleaf = shapehistogramleaf(data, options.soma, options.ANG, options.DIS, thr);

catch e
    disp(e.message)
    keyboard;
end
if ~all(cellfun(@(x)all(isfinite(x(:))),struct2cell(X))) || ...
        any(cellfun(@(x)any(imag(x(:))),struct2cell(X)))
    warning('feature vector has NaN or Inf or complex value')
    keyboard;
end
end

