function [ options ] = get_options( options, useroptions )
%GET_OPTIONS get user-defined parameters, otherwise get default parameters
%   input
%	defaultoptions		default options
%	options				user-given options
%
%	Output
%	options				user-given options filled with default option
%

%# read the acceptable names
optionNames = fieldnames(options);

%# count arguments
nArgs = length(useroptions);
if round(nArgs/2)~=nArgs/2
    error('EXAMPLE needs propertyName/propertyValue pairs')
end

for pair = reshape(useroptions,2, []) %# pair is {propName;propValue}
	inpName = lower(pair{1}); %# make case insensitive
	if any(strcmp(inpName, optionNames))
        %# overwrite options. If you want you can test for the right class here
        %# Also, if you find out that there is an option you keep getting wrong,
        %# you can use "if strcmp(inpName,'problemOption'),testMore,end"-statements
        options.(inpName) = pair{2};
	else
        error('%s is not a recognized parameter name', inpName);
	end
end
end