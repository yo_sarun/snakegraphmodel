function [S, C] = connectcomp(G)
%CONNECTCOMP find connected component in graph G
%need matlab_bgl from matlab file exchange
%   Input
%	G		graph in sparse matrix
%
%	Output
%	S		number of components
%	C		component number
%

[C, sizes] = components(G);
C = C';
S = length(sizes);

% n = size(G,1);
% C = zeros(1,n);
% S = 0;  
% visit = false(1,n);
% for i = 1:n
%     if visit(i)
%         continue
%     end
%     [~, dt, ~] = bfs(G,i);
%     ind = dt>0;
%     visit(ind) = true;
%     % update segment S, C
%     S = S + 1;
%     C(ind) = S;
% end
end

