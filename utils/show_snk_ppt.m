function show_snk_ppt( Vpre, snakes, collide, scale, change, MAXCON )
%SHOW_SNK_PPT show snake for making video for presenting

% get color map of snakes
if nargin >= 3 && ~isempty(collide)
    comp = components(collide+collide');
    palette = jet(max(comp));
    colmap = palette(comp,:);
else
    colmap = jet(length(snakes));
end


smp = 0:0.2:2*pi;

% draw image
subplot(1,2,1);
imagesc(mean(Vpre, 3)); colormap gray; 

% draw snakes
hold on;
for i = 1:length(snakes)
    tmp = snakes(i).vert;
	plot(tmp(:,1), tmp(:,2), 'LineWidth', 2, 'Color',colmap(i,:), 'LineWidth', 8);
    
    if nargin >= 4 && ~isempty(scale)
        rad = ba_interp3(scale, snakes(i).vert(:,1), snakes(i).vert(:,2), snakes(i).vert(:,3), 'linear');

        px = bsxfun(@plus, tmp(:,1), rad*cos(smp));
        py = bsxfun(@plus, tmp(:,2), rad*sin(smp));
        for j = [1, size(px,1)]
            plot(px(j,:), py(j,:), 'LineWidth', 1, 'Color',colmap(i,:));
        end
    end
end
hold off;
set(gca,'position',[0.05 0.05 .45 .9])
axis off

subplot(1,2,2);
colmap1 = [0 0.4470 0.7410; 0.8500 0.3250 0.0980; 0.4660 0.6740 0.1880];
colcode = 2*ones(size(change));
colcode([snakes.converge]>MAXCON) = 1;
colcode(change) = 3;
for i = 1:length(snakes)
    if i == 2, hold on, end
    tmp = snakes(i).vert;
	plot3(tmp(:,1), tmp(:,2), tmp(:,3), 'Color', colmap1(colcode(i),:), 'LineWidth', 5);
end
hold off;
box on
set(gca, 'Ydir', 'reverse')
view(-20,70)
axis([0 300 0 250 0 14])
end

