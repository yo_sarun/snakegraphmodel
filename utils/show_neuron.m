function h = show_neuron(data, V, varargin)
%SHOW_NEURON display neuron
%   Input
%   data        n-by-7 array for neuron file
%   V           3D image stack
%   varargin    utility options
%
%   Output
%   h           figure number
%

% process input
options = struct('factor',1,'overlay',false,'color',[0, sum(data(:,7)==-1)]);
optionNames = fieldnames(options);

for pair = reshape(varargin,2,[]) % pair is {propName;propValue}
   inpName = lower(pair{1}); % make case insensitive
   if any(strcmp(inpName,optionNames))
      options.(inpName) = pair{2};
   else
      error('%s is not a recognized parameter name',inpName)
   end
end

if ~options.overlay
    h = figure; 
    set(gcf, 'PaperUnits', 'centimeters');
    set(gcf, 'PaperPosition', [0 0 30 30]);
    imshow(max(V,[],3),[]);
else
    h = 0;
end
colmap = jet(options.color(2));
hold on
n = options.color(1);
for i = 1:size(data,1)
    if data(i,7) > 0
        idx = [i, data(i,7)];
        plot(data(idx,3)./options.factor, ...
                data(idx,4)./options.factor, ...
                'Color', colmap(n,:), 'Linewidth', 2);
    else
        n = n+1;
    end
end
plot(data(1,3), data(1,4),'o', 'LineWidth',2,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r',...
        'MarkerSize',10);
hold off
end

