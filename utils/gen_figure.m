function img = gen_figure( im1, im2, map )
%GEN_FIGURE display im1 only the snake part on top of im2 using colormap

img = zeros([size(im1) 3]);
im2 = im2 ./ max(im2(:));
im2 = round(im2 * 254)+1;
for i = 1:size(im1,1),
    for j = 1:size(im1,2),
        if im1(i,j) > 0,
            img(i,j,:) = im1(i,j);
        else
            img(i,j,:) = map(im2(i,j),:);
        end
    end
end

end

