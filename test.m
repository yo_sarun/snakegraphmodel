addpath(genpath('matlab_bgl'))
addpath('metric')
addpath('imagevolume')
addpath('utils');

%% DIADEM
filenames = {'OP_1', 'OP_3', 'OP_4', 'OP_5', 'OP_6', 'OP_7', 'OP_8', 'OP_9', 'CF_1', 'CF_2'};
neuron_type = [5, 5, 5, 5, 5, 5, 5, 5, 1, 1];
ori_dir = 'swc/diadem_gold/';
testdir = {'swc/ours/diadem/crf', 'swc/previous/diadem/yoyon', 'swc/kmst/diadem/gon', 'swc/farsight/diadem/fs', 'swc/nct/diadem/cho', 'swc/pct/diadem/bas', 'swc/neutube/diadem/mye', 'swc/app2/diadem/apptwo'};

h = length(filenames); w = length(testdir);
diadem = zeros(h,w); P = zeros(h,w); R = zeros(h,w); F1 = zeros(h,w); MES = zeros(h,w); MAE = zeros(h,w);
for n = 1:length(filenames)
    result = EV_get_report([ori_dir filenames{n} '.swc'], strcat(testdir(1:end), filenames{n}, '.swc'), ...
            'neuron_type', neuron_type(n));
    diadem(n,:) = result(:,1)';
    P(n,:) = result(:,2)'; R(n,:) = result(:,3)';
    F1(n,:) = result(:,4)';
    MES(n,:) = result(:,5)'; MAE(n,:) = result(:,6)';
end

%% drosophila laevae
filenames = {'72413_h0', '72413_h05', '72413_h2', ...
        '72513_h0', '72513_h1', ...
        '73113_1_h0', '73113_1_h05', '73113_1_h1', ...
        '73113_2_h0', '73113_2_h115', '73113_2_h2'};
ori_dir = 'swc/t';
testdir = {'swc/ours/crf', 'swc/previous/yoyon', 'swc/kmst/gon', 'swc/farsight/fs', 'swc/nct/nct', 'swc/neutube/mye', 'swc/app2/apptwo'};

h = length(filenames); w = length(testdir);
diadem = zeros(h,w); P = zeros(h,w); R = zeros(h,w); F1 = zeros(h,w); MES = zeros(h,w); MAE = zeros(h,w);
for n = 1:length(filenames)
    result = EV_get_report([ori_dir filenames{n} '.swc'], strcat(testdir(1:end), filenames{n}, '.swc'));
    diadem(n,:) = result(:,1)';
    P(n,:) = result(:,2)'; R(n,:) = result(:,3)';
    F1(n,:) = result(:,4)';
    MES(n,:) = result(:,5)'; MAE(n,:) = result(:,6)';
end

%% flycircuit
testdir = {'swc/ours/flycircuit/'};

numlist = [6, 7, 69, 94, 95, 96, 106, 126, 127, 134];

h = length(numlist); w = length(testdir);
diadem = zeros(h,w); P = zeros(h,w); R = zeros(h,w); F1 = zeros(h,w); MES = zeros(h,w); MAE = zeros(h,w);

directory = '~/Desktop/checked7_taiwan_flycirciut/';
savedir = 'swc/ours/flycircuit';

for n = 1:h
    filename = sprintf('ChaMARCM-F%06d_seg001.lsm_c_3', numlist(n));
    savefile = fullfile(savedir, sprintf('ChaMARCM-F%06d_seg001.lsm_c_3.swc', numlist(n)));
%     % read swc
    flist = dir([directory 'uint8_' filename '.tif/*.swc']);
    swcname = [directory 'uint8_' filename '.tif/' flist.name];
    data = read_swc_file(swcname);

    result = EV_get_report(swcname, strcat(testdir(1:end), filename, '.swc'));
    diadem(n,:) = result(:,1)';
    P(n,:) = result(:,2)'; R(n,:) = result(:,3)';
    F1(n,:) = result(:,4)';
    MES(n,:) = result(:,5)'; MAE(n,:) = result(:,6)';
end

% %% region of drosophila laevae
% filenames = {'72413_h0', '72413_h05', '72413_h2', '72513_h0', '72513_h1'};
% ori_dir = '../ImageRegion/orig';
% testdir = {'swc/region/cho', 'swc/region/bas', 'swc/region/roy', 'swc/region/yoyonimg', 'swc/region/crfimg'};
% 
% for n = 1:length(filenames)
%     result = EV_get_report([ori_dir filenames{n} '.swc'], strcat(testdir(1:end), filenames{n}, '.swc'));
% end

%% fix swc from farsight
% V = IV_loadimg('../Neuron/72413/72413_h0/');
% fix_swc('swc/far72413_h0.swc', 'swc/fs72413_h0.swc', V, 1);
% 
% V = IV_loadimg('../Neuron/72513/72513_h0/');
% fix_swc('swc/far72513_h0.swc', 'swc/fs72513_h0.swc', V, 1);
% 
% V = IV_loadimg('../Neuron/73113_1/Late 1st - early 2nd instar/73113_1_L1E2_0/');
% fix_swc('swc/far73113_L1E2_h0.swc', 'swc/fs73113_1_L1E2_h0.swc', V, 1);
% 
% V = IV_loadimg('../Neuron/73113_2/CD8 timelapse - late 3rd instar/73113_2_L3_h0/');
% fix_swc('swc/far73113_2_L3_h0.swc', 'swc/fs73113_2_L3_h0.swc', V, 1);
