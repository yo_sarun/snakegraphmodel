function cursnk = AC_remesh(cursnk,res,method, type)
% AC_REMESH     Remesh an active contour (AC) to ensure the resolution.
%     vertex1 = AC_REMESH(vertex0)
%     vertex1 = AC_REMESH(vertex0,res)
%     vertex1 = AC_REMESH(vertex0,res,method)
%     vertex1 = AC_REMESH(vertex0,res,method,type)
% 
%     Inputs
%     cursnk      a struct of snake consisting of vert, kstr, iter
%     vertex0     position of the vertices, n-by-2 matrix, each row of 
%                 which is [x y]. n is the number of vertices.
%     res         desired resolution (distance between vertices) in pixels,
%                 default value is 1.
%     method      'adaptive' - (default) the distances between vertices
%                           will be larger than res/2 and less than res*2,
%                           the shape will be the same
%                 'equal' - the distances between vertices will be the same, 
%                           the shape may be slightly different after remeshing
%     type        'close' - close contour (default), the last vertex and
%                           first vertex are connected 
%                 'open'  - open contour,  the last vertex and first vertex
%                           are not connected 
%     Outputs
%     vertex1     position of the vertices after remeshing, m-by-2 matrix
%     
%     Example
%         See EXAMPLE_VFC, EXAMPLE_PIG.
%
%     See also AMT, AC_DEFORM, AM_VFC, AM_VFK, AM_PIG, AC_INITIAL, 
%     AC_DISPLAY, AM_GVF, EXAMPLE_VFC, EXAMPLE_PIG. 
% 
%     Reference
%     [1] Bing Li and Scott T. Acton, "Active contour external force using
%     vector field convolution for image segmentation," Image Processing,
%     IEEE Trans. on, vol. 16, pp. 2096-2106, 2007.  
%     [2] Bing Li and Scott T. Acton, "Automatic Active Model
%     Initialization via Poisson Inverse Gradient," Image Processing,
%     IEEE Trans. on, vol. 17, pp. 1406-1420, 2008.   
% 
% (c) Copyright Bing Li 2005 - 2009.

% Revision Log
%   11-30-2005  original 
%   02-18-2006  support open type
%   01-30-2009  minor bug fix

%% inputs check
% if ~ismember(nargin, 3:5) || numel(res) ~= 1,
%     error('Invalid inputs to AC_REMESH!')    
% end
if size(cursnk.vert,2) ~= 3 
    error('Invalid vertex matrix!')
end

if nargin<2,
    res = 1;
end    
if nargin<4,
    method = 'adaptive';    
end
if nargin<5,
    type = 'close';    
end
%% delete the vertices close to each other (distance less than res/2)
N = size(cursnk.vert,1);
if N < 3
    return
end
% keep snake points inside image
% cursnk.vert(cursnk.vert<1) = 1;
% cursnk.vert(cursnk.vert(:,1)>s(2),1) = s(2); 
% cursnk.vert(cursnk.vert(:,2)>s(1),2) = s(1); 
% cursnk.vert(cursnk.vert(:,3)>s(3),3) = s(3);
% keyboard;
% dif = diff(cursnk.vert);
% d = sqrt(sum(dif.^2,2));
% idx = find(d<res/2);
% idx(idx==N-1) = N-2; % keep first & last points
% cursnk.vert(idx+1,:) = [];

% while 1
%     N = size(cursnk.vert,1);
%     if N<5,
%         return
%     end
%     p = cursnk.vert(:,1)+sqrt(-1)*cursnk.vert(:,2);    
%     dist = abs(p(:,ones(1,N))-p(:,ones(1,N)).');   % calculate distances between all the vertices
%     [i,j] = find(dist<res/2);
%     i = i(i~=j);    
%     idx = medfilt2(ismember(1:N,i),[1 3]);
%     i = union(find(idx(2:2:end))*2,find(imopen(idx,ones(1,5))));
%    
%     if isempty(i),
%         break           % no more close vertices left
%     end
%     
%     try 
%         cursnk.vert(i,:) = [];
%         cursnk.kstr(i,:) = [];
%         cursnk.k(i,:) = [];
%     catch
%         break
%     end
% end

%% remesh
if strcmp(method,'adaptive'),      
    if strcmp(type,'open'),  
        d = sqrt(sum((cursnk.vert(2:end,:) - cursnk.vert(1:end-1,:)).^2,2));
    else
        d = sqrt(sum((cursnk.vert([2:end 1],:) - cursnk.vert).^2,2));
    end
    while max(d)>res*2, % insert vertices between faraway vertices
        id = find(d>res*2);
        N = size(cursnk.vert,1);
        ind = [1:N,id'+.5];
        cursnk.vert = [cursnk.vert; (cursnk.vert(id,:)+cursnk.vert(mod(id,N)+1,:))/2];
        [~,ix] = sort(ind);
        cursnk.vert = cursnk.vert(ix,:);
        if strcmp(type,'open'),
            d = sqrt(sum((cursnk.vert(2:end,:) - cursnk.vert(1:end-1,:)).^2,2));
        else
            d = sqrt(sum((cursnk.vert([2:end 1],:) - cursnk.vert).^2,2));
        end   
    end
elseif strcmp(method,'equal'),
    M = size(cursnk.vert,1);
    dif = diff(cursnk.vert);
    cursnk.vert(sum(dif,2)==0,:) = [];  % Ensure  strict monotonic order.
    dif(sum(dif,2)==0,:) = [];  % consistent to cursnk.vert
    d = sqrt(sum(dif.^2,2));  % distance between adjacent points
    D = sum(d);
    di = linspace(0,D,M);
    dcum = [0;cumsum(d)];
    vert = zeros(M,3);
    try
    for i=1:3,
        vert(:,i) = interp1(dcum,cursnk.vert(:,i),di)';
    end
    catch
        vert = [];
        cursnk.converge = 1;
    end
    % update vert
    cursnk.vert = vert;
else
    error('Invalid method!');
end