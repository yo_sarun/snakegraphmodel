close all;

V = IV_loadimg('../Neuron/72413/72413_h05/');
data = EV_readSWC( 'swc/t72413_h05.swc' );
figure(1); EV_plot( V, data, '2D', [500, 350, 750, 680] );
print(1, '-dpng', 'sample72413_h05_ori.png', '-r300')

data = EV_readSWC( 'swc/previous/myn72413_h05.swc' );
figure(2); EV_plot( V, data, '2D', [500, 350, 750, 680] );
print(2, '-dpng', 'sample72413_h05_prev.png', '-r300')

data = EV_readSWC( 'swc/nct/bas72413_h05.swc' );
figure(3); EV_plot( V, data, '2D', [500, 350, 750, 680] );
print(3, '-dpng', 'sample72413_h05_nct.png', '-r300')

data = EV_readSWC( 'swc/farsight/far72413_h05.swc' );
figure(4); EV_plot( V, data, '2D', [500, 350, 750, 680] );
print(4, '-dpng', 'sample72413_h05_far.png', '-r300')

data = EV_readSWC( 'swc/ours/crf72413_h05.swc' );
figure(5); EV_plot( V, data, '2D', [500, 350, 750, 680] );
print(5, '-dpng', 'sample72413_h05_our.png', '-r300')


%%
close all;

V = IV_loadimg('../Neuron/72513/72513_h1/');
data = EV_readSWC( 'swc/t72513_h1.swc' );
figure(1); EV_plot( V, data, '2D', [50, 250, 350, 650] );
print(1, '-dpng', 'sample72513_h1_ori.png', '-r300')

data = EV_readSWC( 'swc/previous/yoyon72513_h1.swc' );
figure(2); EV_plot( V, data, '2D', [50, 250, 350, 650] );
print(2, '-dpng', 'sample72513_h1_prev.png', '-r300')

data = EV_readSWC( 'swc/nct/bas72513_h1.swc' );
figure(3); EV_plot( V, data, '2D', [50, 250, 350, 650] );
print(3, '-dpng', 'sample72513_h1_nct.png', '-r300')

data = EV_readSWC( 'swc/farsight/fs72513_h1.swc' );
figure(4); EV_plot( V, data, '2D', [50, 250, 350, 650] );
print(4, '-dpng', 'sample72513_h1_far.png', '-r300')

data = EV_readSWC( 'swc/ours/crf72513_h1.swc' );
figure(5); EV_plot( V, data, '2D', [50, 250, 350, 650] );
print(5, '-dpng', 'sample72513_h1_our.png', '-r300')


%% 
P = [0.1531    0.7944
    0.1586    0.7929
    0.1971    0.7824
    0.2927    0.7565
    0.4216    0.7221
    0.5455    0.6901
    0.6485    0.6612
    0.7259    0.6317
    0.7796    0.5958
    0.8160    0.5467
    0.8328    0.4876
    0.8209    0.4291
    0.7792    0.3805
    0.7161    0.3477
    0.6443    0.3297
    0.5830    0.3140
    0.5416    0.2887
    0.5081    0.2446
    0.4736    0.1711
    0.4456    0.0510];
X = cell(1, size(P,1)-1);
Y = cell(1, size(P,1)-1);

img = zeros(200);
P = round(P*100);
P(:,2) = 100 - P(:,2);
P = P + 50;
for i = 1:size(P,1)-1,
    [X{i},Y{i},~] = bresenham_line3d([P(i,:), 1], [P(i+1,:), 1]);
end

px = [X{:}];
py = [Y{:}];
p = [px; py]';
p(sum(abs(diff(p)),2)==0,:) = [];

img(sub2ind(size(img), p(:,2), p(:,1))) = 5;
h = fspecial('gaussian', [3, 3], 3);
f = imfilter(img, h);

% figure, imshow(f);
[D, S] = bwdist(img > 0);
idx = sub2ind(size(img), p(:,2), p(:,1));
figure;

%% F
t = zeros(size(img));
np = length(idx)-1;
for i = 1:length(idx)
    t(S == idx(i)) = (i-1)/np;
end
F = exp(-100*t) + exp(-100*(1-t));
subplot(2,3,2); imshow(gen_figure( f, F, jet(255) ))
colormap jet;
caxis([0 1])

%% G
costheta = zeros(size(img));
dp = zeros(size(p));
dp(1,:) = p(1,:) - p(4,:);
dp(1,:) = dp(1,:) ./ sqrt(sum(dp(1,:).^2));
dp(end,:) = p(end,:) - p(end-3,:);
dp(end,:) = dp(end,:) ./ sqrt(sum(dp(end,:).^2));
dp = [dp; dp(end,:)];
for i = [1 length(idx)],
    [sy, sx] = find(S == idx(i));
    v = [sx - p(i,1), sy - p(i,2)];
    v = v ./ (max(sqrt(sum(v.^2,2)), 0.0001) * ones(1,2));
    
    costheta(S == idx(i)) = max(costheta(S==idx(i)), abs(v*dp(i,:)'));
end
G = max(costheta, 0.5);
subplot(2,3,1); imshow(gen_figure( f, G, [zeros(127,3); jet(128)] ));
colormap jet;
caxis([0.5 1])

%% D
Dmax = max(D(:));
subplot(2,3,4); imshow(gen_figure( f, D, jet(255) ))
colormap jet;
caxis([0,Dmax])

d = 1./(1+exp(10*(min(costheta,0.5)-0.5)));
subplot(2,3,5); imshow(gen_figure( f, d, [zeros(127,3); jet(128)] ))
colormap jet;

subplot(2,3,3); imshow(gen_figure( f, d.*D, jet(255) ))

%% I
I = (G.*F)./((d.*D)+20);
subplot(2,3,6); imshow(gen_figure( f, I, jet(255) ))