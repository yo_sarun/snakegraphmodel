% This function eliminates all intermediate vertices from undirected AMlbl and returns
% the result in a labeled directed matrix of the same size. The output has to
% be directed to encode two vertex loops.

function AMlbl_topology = Eliminate_Intermediate_Vertices(AMlbl)

AMlbl_topology = sparse(size(AMlbl,1),size(AMlbl,2));
AM=(AMlbl>0);
L=unique(AMlbl(AM));

% connect branch or end vertices directionally using the branch label
for i=1:length(L)
    [e1 e2]=find(AMlbl==L(i));
    temp=e1(sum(AM(:,e1))==1 | sum(AM(:,e1))>=3);
    if AMlbl_topology(temp(1),temp(end))==0
        AMlbl_topology(temp(1),temp(end))=L(i);
    else
        AMlbl_topology(temp(end),temp(1))=L(i);
    end
end



