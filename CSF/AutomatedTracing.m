% This function performs automated tracing of neurites in a thresholded image, Im

function [AMlbl_merged,r_merged]=AutomatedTracing(Im,Start_X,Start_Y,Start_Z, Parameters)
disp(['There are ',num2str(nnz(Im)), ' voxels in the thresholded image.'])

sizeIm=size(Im);

temp=Start_X;
Start_X=Start_Y;
Start_Y=temp;

Start_X=Start_X+1;
Start_Y=Start_Y+1;
Start_Z=Start_Z+1;

% 1. Voxel Coding
Start_X1=fix(Start_X);
Start_Y1=fix(Start_Y);
Start_Z1=fix(Start_Z);

if Im(Start_X1,Start_Y1,Start_Z1)==0
    ind=find(double(Im));
    [xx,yy,zz]=ind2sub_AS(sizeIm,ind);
    rr=((xx-Start_X).^2+(yy-Start_Y).^2+(zz-Start_Z).^2).^0.5;
    [temp,ind]=min(rr);
    Start_X1=xx(ind);
    Start_Y1=yy(ind);
    Start_Z1=zz(ind);  
end
clear xx yy zz rr

disp('Voxel Coding started.')
W = VoxelCoding(Im,Start_X1,Start_Y1,Start_Z1,Parameters.W);
disp('Voxel Coding is complete.')

% 2. Create sparse AM from W
[AM,r] = W2AM(Im,W,[]);
clear W
disp('Adjacency matrix is created.')

% 3. Label branches in AM
AMlbl = LabelBranchesAM(AM);
disp('Adjacency matrix is labeled.')

% Error check
L=unique(AMlbl(AMlbl>0));
for i=1:length(L)
    if Loops(AMlbl==L(i))==1
        AMlbl(AMlbl==L(i))=0;
        disp('Loop detected')
    end
end
temp_ind=(sum(AMlbl)~=0);
AMlbl=AMlbl(temp_ind,temp_ind);
r=r(temp_ind,:);
disp(['There are ',num2str(length(unique(AMlbl(AMlbl>0)))), ' branches in the image.'])

% ??. Eliminate Short Vertical Intermediate Branches
for i=2:2:Parameters.ShortIntermediateBranchLength
    [AMlbl,r] = Eliminate_Vertical_Intermediate_Branches(AMlbl,r,i);
    disp([num2str(length(unique(AMlbl(AMlbl>0)))), ' branches remain after short vertical intermediate branch elimination.'])
end

% 4. Eliminate Short Terminal Branches
for thr=1:0.5:5
    [AMlbl r ~] = Eliminate_Terminal_Branches(AMlbl,r,thr,1,1); 
    disp([num2str(length(unique(AMlbl(AMlbl>0)))), ' branches remain after short terminal branch elimination.'])
end

% 5. Eliminate Small Loops
disp('Eliminating small loops.')
[AMlbl, r, ~, ~]=Reduce_Small_Loops(AMlbl,r,Parameters.LongBranchLength);
disp([num2str(length(unique(AMlbl(AMlbl>0)))), ' branches remain after small loop elimination.'])
   
% 6. Eliminate Short Terminal Branches
for thr=5.5:0.5:Parameters.ShortTerminalBranchLength
    [AMlbl r ~] = Eliminate_Terminal_Branches(AMlbl,r,thr,1,1); 
    disp([num2str(length(unique(AMlbl(AMlbl>0)))), ' branches remain after short terminal branch elimination.'])
end
 
% 7. Reduce or Eliminate Short Intermediate Branches
for i=2:2:Parameters.ShortIntermediateBranchLength
    [AMlbl,r] = Reduce_Short_Intermediate_Branches(AMlbl,r,i);
    %[AMlbl,r] = Eliminate_Short_Intermediate_Branches(AMlbl,r,i);
    disp([num2str(length(unique(AMlbl(AMlbl>0)))), ' branches remain after short intermediate branch reduction.'])
end

% 8. Cut the trace near the xy faces of the stack
%edge_points=(r(:,1)<=Parameters.trim2(1) | r(:,1)>sizeIm(1)-Parameters.trim2(2) | r(:,2)<=Parameters.trim2(2) | r(:,2)>sizeIm(2)-Parameters.trim2(4));
edge_points=(r(:,1)<=Parameters.trim | r(:,1)>sizeIm(1)-Parameters.trim | r(:,2)<=Parameters.trim | r(:,2)>sizeIm(2)-Parameters.trim);

if nnz(edge_points)>0
    AMlbl(edge_points,:)=[];
    AMlbl(:,edge_points)=[];
    r(edge_points,:)=[];
    AMlbl = LabelBranchesAM(AMlbl>0);
end

% 9. Optimize the trace
disp('Trace optimization started.')
[AMlbl r I_snake]=Snake_All_norm(Im,AMlbl,r,1,0,0,Parameters.pointsperum,Parameters.Nstep,Parameters.alpha,Parameters.betta,Parameters.sig_ips,Parameters.sig_bps,Parameters.sig_tps,1);
%[AMlbl r I_snake]=Snake_All_curvature_norm(Im,AMlbl,r,1,1,0,Parameters.pointsperum,Parameters.Nstep*3,Parameters.alpha,Parameters.betta,Parameters.sig_ips,Parameters.sig_bps,Parameters.sig_tps,1);
disp('Trace optimization is complete.')

% This optimization is optional. It is beter to do itin 2 steps: low ppm and then higher ppm
% [AMlbl2 r2 I_snake]=Snake_All_curvature_norm(Im,AMlbl1,r1,1,0,0,0.25,50,0.2,0.05,1,2,2,1);
% [AMlbl3 r3 I_snake]=Snake_All_curvature_norm(Im,AMlbl2,r2,1,1,0,0.5,50,0.2,0.05,1,2,2,1);
% can use 0,0,0 instead of 1,1,0

MeanI0=mean(I_snake);

% 10. Disconnect all branches at branch points
if Parameters.Disconnect==1
    bps=find(sum((AMlbl>0))>=3);
    AMlbl_temp=AMlbl;
    AMlbl_temp(AMlbl_temp==1)=NaN;
    bps(isnan(sum(AMlbl_temp(:,bps))))=[]; % exclude the Root
    if nnz(bps)>0
        AMlbl(bps,:)=[];
        AMlbl(:,bps)=[];
        r(bps,:)=[];
        AMlbl = LabelBranchesAM(AMlbl>0);
    end
end

if length(unique(AMlbl(AMlbl>0)))~=1
    % 11. Calculate the cost components
    disp('Calculation of cost components started.')
    [Dist,Cos,Offset,MeanI,CVI] = MergingCosts(Im,AMlbl,r,Parameters);
    disp('Calculation of cost components is complete.')
    
    % 12. Automated Branch Merger
    disp('Automated merger started.')
    AMlbl_merged=AutomatedMerger(Im,AMlbl,r,Dist,Cos,Offset,MeanI,CVI,MeanI0,Parameters);
    disp('Automated merger is complete.')
    
    % 13. Optimize the trace
    disp('Optimization of the merged trace started.')
    [AMlbl_merged, r_merged, ~]=Snake_All_norm(Im,AMlbl_merged,r,1,1,1,Parameters.pointsperum,Parameters.Nstep,Parameters.alpha,Parameters.betta,Parameters.sig_ips,Parameters.sig_bps,Parameters.sig_tps,1);
    % can use 1,1,1 instead of 0,0,1
    disp('Optimization of the merged trace is complete.')
else
    AMlbl_merged=AMlbl;
    r_merged=r;
end
    
% 14. Eliminate Small Trees
TreeLabels=unique(AMlbl_merged(AMlbl_merged>0));
N_trees=length(TreeLabels);
for i=1:N_trees
    [ii,jj]=find(AMlbl_merged==TreeLabels(i));
    L_tree=sum(sum((r_merged(ii,:)-r_merged(jj,:)).^2,2).^0.5)./2;
    if L_tree<=Parameters.SmallTreeLength
        AMlbl_merged(ii,jj)=0;
    end
end
cut=(sum(AMlbl_merged)==0);
if nnz(cut)>0
    AMlbl_merged(cut,:)=[];
    AMlbl_merged(:,cut)=[];
    r_merged(cut,:)=[];
    AMlbl_merged = LabelTreesAM(AMlbl_merged);
end
disp(['There are ',num2str(length(unique(AMlbl_merged(AMlbl_merged>0)))), ' trees in the stack.'])
    
