% Function needed for MatLab - Java data format conversion.

function Save_data(filepath,Original,IM,AM,R,Red_Type,Red_Fac)

if ~isempty(AM)
    rem_ind=(sum(AM)==0);
    AM(rem_ind,:)=[];
    AM(:,rem_ind)=[];
    R(rem_ind,:)=[];
else
    AM=[];
    R=[];
end

save(filepath,'Original','IM','AM','R','Red_Type','Red_Fac')