direc = 'swc/previous/';
l = dir([direc '*.swc']);


for i = 1:length(l)
    fname = [direc l(i).name];
    data = EV_readSWC(fname);
    data(:,3:5) = data(:,3:5) - 1;
    fileID = fopen(fname, 'w');
    fprintf(fileID,'%d %d %.3f %.3f %.3f %.4f %d\n',data');
    fclose(fileID);
    disp(['Save SWC file: ' fname ' completed...'])
end