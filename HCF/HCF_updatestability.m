function [stability, graph, snkgrid] = HCF_updatestability(...
        snakes, stability, graph, snkgrid, change, BLOCKSIZE, MAXCON, ZRATIO)
%HCF_UPDATESTABILITY update the stability of HCF and graph based on the new
%configuration of snakes and previous stability and graph of graphical
%model. Graph may change when snakes merge (put them into the same site).

%% group snakes into grid whose cell is 10-by-10
n = length(snakes);
grididx = zeros(0,3);
gridsize = size(snkgrid);

graphdeg = full(sum(graph));
for i = 1:n
    if ~change(i), continue, end
    
    if snakes(i).converge > MAXCON
        % snake energy doesn't decrease for 3 iters, stop deform this snake
        disp(['snake ' num2str(i) ' converge']);
        stability(i) = 0;
    else
        % stability: prefer short snk with low energy, distance + energy
        dsnk = sum(sqrt(sum(diff(snakes(i).vert).^2,2)));
%         stability(i) = snakes(i).E/dsnk;
        stability(i) = snakes(i).E/(dsnk*graphdeg(i));
    end
    % connect to snake within 10 pixels, approx by divide into grid 10x10
    visited = false(gridsize);
    snkcells = ceil(snakes(i).vert/BLOCKSIZE);
    for snkcell = snkcells'
        if visited(snkcell(2), snkcell(1), snkcell(3)), continue, end
        visited(snkcell(2), snkcell(1), snkcell(3)) = true;
        snkgrid{snkcell(2), snkcell(1), snkcell(3)} = ...
                union(snkgrid{snkcell(2), snkcell(1), snkcell(3)}, i);
        grididx = union(grididx, [snkcell(2), snkcell(1), snkcell(3)], 'rows');
    end
end

%% update graph
scan = -1:1:1;
[delta_x, delta_y, delta_z] = meshgrid(scan, scan, scan);
delta = [delta_x(:), delta_y(:), delta_z(:)];
max_size = ones(27,1) * gridsize;

for block = grididx'
    blocks = (ones(27,1) * block') + delta;
    % check points are inside image
    cond = all(blocks >= 1 & blocks <= max_size, 2);
    idx = blocks(cond,1) + (blocks(cond,2)-1)*gridsize(1) + (blocks(cond,3)-1)*gridsize(1)*gridsize(2);
    % check adjacent snakes in same of neighbour cells
    visited = spalloc(n,n,10);
    C = [snkgrid{idx}];
    for Ci = C
        if ~change(Ci), continue, end
        for Cj = C
            % if same snake or already connected or checked, ignore it
            if Ci == Cj || visited(Ci,Cj) || graph(Ci,Cj), continue, end
            visited(Ci,Cj) = true;
            
            % compare two snk are adjacent only at end-points of snake i,j
            si = Ci; sj = Cj;
            for l = 1:2
                for k = [1 size(snakes(si).vert,1)]
                    ds = bsxfun(@minus, snakes(sj).vert, snakes(si).vert(k,:));
                    ds = bsxfun(@times, ds, [1,1,ZRATIO]);   % scale along-z
                    if any(sqrt(sum(ds.^2,2)) <= BLOCKSIZE)
                        graph(Ci,Cj) = 1;
                        graph(Cj,Ci) = 1;
                        break
                    end
                end
                if graph(Ci,Cj), break; end
                % swap i and j in second iteration
                si = Cj; sj = Ci;
            end
        end
    end
end
end