function [ stability, graph, snkgrid ] = HCF_initstability( snakes, V, BLOCKSIZE )
%HCF_INITSTABILITY initialize the stability of HCF (Highest Confidence
%First) based on the configuration of snakes. The function also computes
%the graph of graphical model as adjacency list (stored in cell array).

n = length(snakes);
% init stability
stability = zeros(1,n);
% each node is the root of its own component
graph = spalloc(n,n,5*n);
for i = 1:n
    graph(i,i) = 1;
end
% init snake grid for updating snake CRF, store in row,col,dep-coordinate
s = max([ceil(size(V)/BLOCKSIZE); 2, 2, 2]);
snkgrid = cell(s);
end

