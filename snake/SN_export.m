function data = SN_export(snakes, edges, K, savefile)
%SN_EXPORT convert snake into swc format

% get number of snake components
n = length(snakes);
A = sparse([edges(:,1) edges(:,2)], [edges(:,2) edges(:,1)], 1, n, n);
ci = components(A);

% plot snake component ordered by its total length
nCC = max(ci);
lenCC = zeros(1,nCC);
for i = 1:nCC
    lenCC(i) = sum(arrayfun(@(x)size(x.vert,1),snakes(ci==i)));
end
[lenCC,CC] = sort(lenCC, 'descend');

% convert each snake component
dataCC = cell(1,K);
visited = false(1,sum(lenCC));
K = min(K, nCC);
for k = 1:K
    % convert snake to graph with set of nodes and edges G = {N,E}
    nk = lenCC(k);
    N = zeros(nk,3);
    edgeE = zeros(nk-1,2);
    curN = 1;
    curE = 1;
    snkpos = zeros(1,n);
    % get nodes and edges inside snake
    for snk = snakes(ci==CC(k))
        snkpos(snk.num) = curN;
        ni = size(snk.vert,1);
        N(curN:curN+ni-1,:) = snk.vert;
        edgeE(curE:curE+ni-2,:) = [curN:curN+ni-2; curN+1:curN+ni-1]';
        curN = curN + ni;
        curE = curE + ni - 1;
    end
    
    % get edges between snakes
    for i = 1:size(edges,1)
        n1 = edges(i,1);
        if ci(n1) == CC(k)
            p1 = edges(i,3);
            n2 = edges(i,2);
            p2 = edges(i,4);
            edgeE(curE,:) = [snkpos(n1)+p1-1,snkpos(n2)+p2-1];
            curE = curE + 1;
        end
    end
    E = sparse([edgeE(:,1); edgeE(:,2)], [edgeE(:,2); edgeE(:,1)], 1, nk, nk);

    % sort points by discover time
    [order,pred] = bfs_traverse(E, 1);
    mat = [(1:nk)', 2*ones(nk,1), N, ones(nk,2)];
    mat = mat(order,:);
    mat(:,7) = pred(order);
    % get fixed index
    fixind = zeros(size(order));
    fixind(order) = 1:length(order);
    fixind = fixind + sum(visited);
    % change to fixed index
    mat(:,1) = fixind(mat(:,1));
    mat(1,7) = -1;
    mat(2:end,7) = fixind(mat(2:end,7));
    
    % store result
    dataCC{k} = mat;
    visited(fixind) = true;
end
data = vertcat(dataCC{:});

%% write to file
if nargin >= 4
    fileID = fopen(savefile, 'w');
%     fprintf(fileID,'# CRF snake to SWC conversion by Sarun Gulyanon.\n');
    fprintf(fileID,'%d %d %.3f %.3f %.3f %.4f %d\n',data');
    fclose(fileID);
    disp('Save SWC file completed...')
end
end
