function SN_show(Vpre, snakes, collide, scale, rect)
%SN_SHOW plot snakes on top of volume in 2D, coloured by snake no. or snake
%components

if nargin < 5
    rect = [1, 1, size(Vpre,1), size(Vpre,2)];
end

% get color map of snakes
if nargin >= 3 && ~isempty(collide)
    comp = components(collide+collide');
    palette = autumn(max(comp));
    colmap = palette(comp,:);
else
    colmap = autumn(length(snakes));
end


smp = 0:0.2:2*pi;

% draw image
% imshow(max(Vpre(rect(1):rect(3), rect(2):rect(4), :), [], 3), [], 'Border', 'tight');% colormap gray
imshow(mean(Vpre(rect(1):rect(3), rect(2):rect(4), :), 3), [], 'Border', 'tight');
% draw snakes
hold on;
for i = 1:length(snakes)
    maxsize = ones(size(snakes(i).vert,1), 1) * [rect(4)-rect(2)+1, rect(3)-rect(1)+1, size(Vpre,3)];
    tmp = bsxfun(@minus, snakes(i).vert, [rect(2)-1, rect(1)-1, 0]);
    
    idx = ~any(tmp < 1 | tmp > maxsize, 2);
    plot(tmp(idx,1), tmp(idx,2), ...
            'LineWidth', 2, 'Color',colmap(i,:));
%     plot(snakes(i).vert(idx,1), snakes(i).vert(idx,2), ...
%             'LineWidth', 2, 'Color',colmap(i,:));
    
    if nargin >= 4 && ~isempty(scale)
%         rad = ba_interp3(scale, snakes(i).vert(:,1), snakes(i).vert(:,2), snakes(i).vert(:,3), 'linear');
% 
%         px = bsxfun(@plus, snakes(i).vert(:,1), rad*cos(smp));
%         py = bsxfun(@plus, snakes(i).vert(:,2), rad*sin(smp));
        rad = ba_interp3(scale, snakes(i).vert(idx,1), snakes(i).vert(idx,2), snakes(i).vert(idx,3), 'linear');

        px = bsxfun(@plus, tmp(:,1), rad*cos(smp));
        py = bsxfun(@plus, tmp(:,2), rad*sin(smp));
        for j = 1:size(px,1)
            plot(px(j,:), py(j,:), ...
                'LineWidth', 1, 'Color',colmap(i,:));
        end
    end
end
hold off;
end

