function opensnk = SN_initsnake(V, seedp, Vx, Vy, Vz, ALPHA, BETA, GAMMA, ZETA)
%INITSNAKE initialize snakes from preprocess volume and seed points by find
%direction along neuron using Frangi Filter. Seed points are in
%[row,column,depth]-coordinate

% for each seed point
opensnk = cell(1,size(seedp,1));
s = size(V);
max_size = ones(3,1) * s([2,1,3]);
cur = 1;
for i = 1:size(seedp,1)
    p = seedp(i,:);
    dv = round([Vx(p(2),p(1),p(3)), Vy(p(2),p(1),p(3)), Vz(p(2),p(1),p(3))]);
    
    vert = [p(1)-dv(1), p(1), p(1)+dv(1); ...
            p(2)-dv(2), p(2), p(2)+dv(2); ...
            p(3)-dv(3), p(3), p(3)+dv(3)]';
    % keep points inside image
    vert(vert < 1) = 1;
    vert(vert > max_size) = max_size(vert > max_size);
    
    Esnk = SN_energy(V, vert, false(1,3), zeros(1,2), ALPHA, BETA, GAMMA, ZETA);

    if Esnk >= 0
        % remove snake with energy >= 0
        continue
    end
    
    % pick the best initial configuration
    opensnk{cur} = struct(...
            'vert',vert,...             % points on snake
            'E', Esnk,...               % snake energy
            'minE', Esnk,...            % snake max energy
            'collide', false(1,2),...   % collision at endpoints
            'num', cur,...              % snake number
            'CClen', 2,...              % length of its snake component
            'CCno', cur,...             % snake component number
            'converge', 0);             % # iterations that snake converges
    cur = cur + 1;
end
out = sprintf('Seed points = %d, Number of snakes = %d (Remove %d snakes)', ...
        size(seedp,1), cur-1, size(seedp,1)-cur+1);
disp(out);
opensnk = [opensnk{:}];         % merge cell array into array of snk struct
end

