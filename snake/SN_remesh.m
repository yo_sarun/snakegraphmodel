function [ vert, overlap_arr ] = SN_remesh( snake_pnts, overlap )
%SN_REMESH remesh snake points

n = size(snake_pnts,1);
remesh_pnts = cell(1,n);
overlap_pnts = cell(1,n);
% add points between snake points
for k=1:n-1
    [X,Y,Z] = bresenham_line3d(snake_pnts(k,:), snake_pnts(k+1,:));
    remesh_pnts{k} = [X(1:end-1)', Y(1:end-1)', Z(1:end-1)'];
    overlap_pnts{k} = overlap(k)*ones(1,size(remesh_pnts{k},1));
end
remesh_pnts{n} = snake_pnts(n,:);
overlap_pnts{n} = overlap(n);
vert = vertcat(remesh_pnts{:});
overlap_arr = logical([overlap_pnts{:}]);

% remove loop in snake curve
[~,~,ic] = unique(vert,'rows');
k = 1;
while k < size(vert,1)
    cur = find(ic==ic(k),1,'last');
    if k+1 < cur
        vert(k+1:cur,:) = [];
        ic(k+1:cur,:) = [];
        overlap_arr(k+1:cur) = [];
    end
    k = k + 1;
end

% remove shape corners
dvert = diff(vert);
keep = true(1,size(vert,1));
k = 2;
while k < size(vert,1) 
    direction = dvert(k,:) + dvert(k-1,:);
    if all(abs(direction) < 2)
        keep(k) = false;
        k = k + 2;
    else
        k = k + 1;
    end
end
vert = vert(keep,:);
overlap_arr = overlap_arr(keep);

end

