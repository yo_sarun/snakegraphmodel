function [snk, collide] = SN_deform(snk, nbhd_snk, V, collide, scale, ...
        ALPHA, BETA, GAMMA, ZETA, MAXV, MAXCON, ZRATIO)
%SN_DEFORM deform an open active contour model (snake) using greedy
%algorithm based on 'A Fast Active Contour Algorithm' by DJ Williams 1991
% **TO DO** need to add hard constraints and interaction energy, fix bugs,
% remesh (must keep number of points low for performance).

%% connect to all neighbours, and collide both ends <=> converge
if all(snk.collide) && all([nbhd_snk.CCno]==snk.CCno)
    snk.converge = MAXCON+1;
    return;
end

%% initialize parameters
snake_pnts = snk.vert;
n = size(snake_pnts,1);

max_delta_x = 1; max_delta_y = 1; max_delta_z = 1;
resol_x = 1; resol_y = 1; resol_z = 1;

alpha = ALPHA*ones(1,n);
beta = BETA*ones(1,n);
gamma = GAMMA*ones(1,n);
zeta = ZETA*ones(1,n);

% initialize variables
scan_x = -max_delta_x:resol_x:max_delta_x;
scan_y = -max_delta_y:resol_y:max_delta_y;
scan_z = -max_delta_z:resol_z:max_delta_z;

[delta_x, delta_y, delta_z] = meshgrid(scan_x, scan_y, scan_z);
num_states = numel(delta_x);
% put the original point first (first peferrence if E is the same)
neu = ceil(num_states/2);
delta = [delta_x([neu 1:neu-1 neu+1:end])', ...
        delta_y([neu 1:neu-1 neu+1:end])', ...
        delta_z([neu 1:neu-1 neu+1:end])'];

s = size(V);
max_size = ones(num_states,1) * s([2,1,3]);

dbar = mean(sqrt(sum(diff(snake_pnts).^2,2)));

%% move points to new location
% for each snake point
overlap = false(1,n);
Eattact = zeros(1,2);
for k = 1:n,
    % generate possible points
    pntsk = (ones(num_states,1) * snake_pnts(k,:)) + delta;
    % check points inside image
    condVk = all(pntsk >= 1 & pntsk < max_size, 2);
    % check points does not shink snake
    if k == 1
        dprev = snake_pnts(k+1,:) - snake_pnts(k,:);
    elseif k == n
        dprev = snake_pnts(k-1,:) - snake_pnts(k,:);
    end
    condpk = sum(abs(bsxfun(@minus, delta, sign(dprev))),2) <= 1 & sum(abs(delta),2)~=0;
    
    condk = condVk & ~condpk;
    
    % get Eimg
    idx = sub2ind(size(V), pntsk(condk,2), pntsk(condk,1), pntsk(condk,3));
    Eimg = inf(num_states, 1);
    for nb_snk = nbhd_snk
        ds = bsxfun(@minus, nb_snk.vert, snake_pnts(k,:));
        overlap(k) = any(all(abs(ds)<=1,2));
        if overlap(k), break, end
    end
    if overlap(k)
        Eimg(condk) = MAXV;
        Eimg(1) = 0;
    else
        Eimg(condk) = V(idx);
    end
    
    % get Econt; encourage even spacing of points
    %** may speed up by compute only at condk points **
    if k == 1
        u0 = bsxfun(@minus,snake_pnts(2,:),pntsk);
        Econt = dbar - sqrt(sum(u0.^2,2));
    elseif k == n
        u0 = bsxfun(@minus,pntsk,snake_pnts(n-1,:));
        Econt = dbar - sqrt(sum(u0.^2,2));
    else
        u0 = bsxfun(@minus,pntsk,snake_pnts(k-1,:));
        Econt = abs(dbar - sqrt(sum(u0.^2,2)));
    end
    
    % get Ecurv; normalize u0, u1 before taking diff
    if k == 1
        u1 = snake_pnts(3,:) - snake_pnts(2,:);
        u1 = u1 / norm(u1);
        u1(isnan(u1)) = inf;
        u0 = bsxfun(@rdivide, u0, sqrt(sum(u0.^2,2)));
        u0(isnan(u0)) = inf;
        Ecurv = sum(bsxfun(@minus, u0, u1).^2,2);
    elseif k == n
        u1 = snake_pnts(n-1,:) - snake_pnts(n-2,:);
        u1 = u1 / norm(u1);
        u1(isnan(u1)) = inf;
        u0 = bsxfun(@rdivide, u0, sqrt(sum(u0.^2,2)));
        u0(isnan(u0)) = inf;
        Ecurv = sum(bsxfun(@minus, u1, u0).^2,2);
    else
        u1 = snake_pnts(k+1,:) - snake_pnts(k,:);
        u1 = u1 / norm(u1);
        u1(isnan(u1)) = inf;
        u0 = bsxfun(@rdivide, u0, sqrt(sum(u0.^2,2)));
        u0(isnan(u0)) = inf;
        Ecurv = sum(bsxfun(@minus, u0, u1).^2,2);
    end
    
    % get Eatt: attraction energy (only at endpoint)
    Eatt = zeros(27,1);

    % find minimal move
    [~,pos] = min(alpha(k)*Econt + beta(k)*Ecurv + gamma(k)*Eimg + zeta(k)*Eatt);
%     disp([alpha(k)*Econt, beta(k)*Ecurv, gamma(k)*Eimg, zeta(k)*Eatt]);

    if k == 1,
        Eattact(1) = Eatt(pos);
    elseif k == n
        Eattact(2) = Eatt(pos);
    end
    % move snake point to local optimal location
    snake_pnts(k,:) = snake_pnts(k,:) + delta(pos,:);
end

%% remesh snake
[vert, overlap_arr] = SN_remesh(snake_pnts, overlap);

%% find collision with neighbour snakes
arr = [1, size(vert,1)];
radii = [scale(vert(arr(1),2), vert(arr(1),1), vert(arr(1),3)), ...
        scale(vert(arr(2),2), vert(arr(2),1), vert(arr(2),3))];
for k = 1:2
    for nb_snk = nbhd_snk
        ds = bsxfun(@minus,vert(arr(k),:),nb_snk.vert);
%         ds = bsxfun(@times, ds, [1,1,ZRATIO]);   % scale along-z
        if any(sqrt(sum(ds.^2,2)) <= radii(k))
            collide(nb_snk.num) = true;
            snk.collide(k) = true;
        end
    end
end

%% find Esnk
Esnk = SN_energy(V, vert, overlap_arr, Eattact, ALPHA, BETA, GAMMA, ZETA);

% check for snake that shrink to a point, make it converge
if isempty(Esnk)
    snk.converge = MAXCON+1;
    return;
end

%% update snake energy, vertices, convergence
% need to allow a drop in E to capture inhomogenuity in image
snk.E = Esnk;
snk.vert = vert;
if snk.minE <= snk.E
    snk.converge = snk.converge+1;
else
    snk.converge = 0;
end

snk.minE = min(Esnk, snk.minE);

end
