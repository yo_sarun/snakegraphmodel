function SN_graph( G, N, Vpre )
%SN_GRAPH Summary of this function goes here
%   Detailed explanation goes here

[ni,nj] = find(G);
if ~isempty(Vpre),
    imagesc(mean(Vpre,3));
    colormap gray
    % draw line
    hold on;
    for i = 1:size(ni,1)
        idx = [ni(i), nj(i)];
        plot(N(idx,1), N(idx,2), 'LineWidth', 2, 'Color', 'c');
    end
    hold off;
else
    plot3(N(1,1), N(1,2), N(1,3));
    hold on
    for i = 1:size(ni,1)
        idx = [ni(i), nj(i)];
        plot3(N(idx,1), N(idx,2), N(idx,3));
    end
    hold off
end
end

