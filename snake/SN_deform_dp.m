function snk = SN_deform_dp(snk, nbhd_snk, V)
%SN_DEFORM deform an open active contour model (snake) using dynamic
%programming.
% modified from SNAKES
% By Chris Bregler and Malcolm Slaney, Interval Technical Report IRC 1995-017
% Copyright (c) 1995 Interval Research Corporation.
% **TO DO** need to add hard constraints and interaction energy, fix bugs,
% dynamic programming is too slow (use greedy one), remesh (must keep
% number of points low for performance).

% parameters
n = size(snk.vert,1);

max_delta_x = 1; max_delta_y = 1; max_delta_z = 1;
resol_x = 1; resol_y = 1; resol_z = 1;

alpha = 0.02*ones(1,n);
beta = 0.05*ones(1,n);

% initialize variables
scan_x = -max_delta_x:resol_x:max_delta_x;
scan_y = -max_delta_y:resol_y:max_delta_y;
scan_z = -max_delta_z:resol_z:max_delta_z;

[delta_x, delta_y, delta_z] = meshgrid(scan_x, scan_y, scan_z);
delta = [delta_x(:), delta_y(:), delta_z(:)];
num_states = numel(delta_x);

s = size(V);
max_size = ones(num_states,1) * s;

Smat = zeros(n,num_states^2);
Imat = zeros(n,num_states^2);


%% forward pass (search for optimal value)
% S_k(v1,v2) = min S_k-1(v0,v1) + alpha(v1-v0)^2 + beta(v2-2v1+v0)^2 + Eext
% for the first point
pnts0 = (ones(num_states,1) * snk.vert(1,:)) + delta;
cond0 = all(pnts0 > 0 & pnts0 < max_size, 2);
idx = sub2ind(size(V), pnts0(cond0,2), pnts0(cond0,1), pnts0(cond0,3));
Eext = inf(1, num_states);
Eext(cond0) = V(idx);
for v2 = 1:num_states,
    Smat(1,(v2-1)*num_states+1:v2*num_states) = Eext;
end

% for points in the middle
pnts1 = (ones(num_states,1) * snk.vert(2,:)) + delta;
cond1 = all(pnts1 > 0 & pnts1 < max_size, 2);
for k = 2:n-1,
    pnts2 = (ones(num_states,1) * snk.vert(k+1,:)) + delta;
    cond2 = all(pnts2 > 0 & pnts2 < max_size, 2);
    idx = sub2ind(size(V), pnts1(cond1,2), pnts1(cond1,1), pnts1(cond1,3));
    Eext = inf(1, num_states);
    Eext(cond1) = V(idx);
    for v2 = 1:num_states,
        if ~cond2(v2)
            Smat(k,(v2-1)*num_states+1:v2*num_states) = Inf;
            continue
        end
        for v1 = 1:num_states,
            if ~cond1(v1)
                Smat(k,(v2-1)*num_states+v1) = Inf;
                continue
            end
            v0_domain = 1:num_states;
            [y,i] = min( Smat(k-1,(v1-1)*num_states+v0_domain) ...
                    + alpha(k) * sum(bsxfun(@minus,pnts1(v1,:),pnts0).^2, 2)' ...
                    + beta(k) * sum(bsxfun(@minus,pnts2(v2,:)-2*pnts1(v1,:),pnts0).^2, 2)' );
            Smat(k,(v2-1)*num_states+v1) = y+(v1);
            Imat(k,(v2-1)*num_states+v1) = i;
        end;
    end
    pnts0 = pnts1;
    pnts1 = pnts2; cond1 = cond2;
end

% for the last point
idx = sub2ind(size(V), pnts1(cond1,2), pnts1(cond1,1), pnts1(cond1,3));
Eext = inf(1, num_states);
Eext(cond1) = V(idx);
for v1 = 1:num_states,
    v0_domain = 1:num_states;
    [y,i] = min( Smat(n-1,(v1-1) * num_states+v0_domain) ...
            + alpha(n) * sum(bsxfun(@minus,pnts1(v1,:),pnts0).^2, 2)' );
    Smat(n,v1) = y+Eext(v1);
    Imat(n,v1) = i;
end;
% get final answer
[Esnk,final_i] = min(Smat(n,1:num_states));


%% backward pass (retrieve the answer)
snake_pnts = zeros(n,3);
snake_pnts(n,:) = snk.vert(n,:) + delta(final_i,:);
v1 = final_i; v2 = 1;
for k=n-1:-1:1,
    v = Imat(k+1,(v2-1)*num_states+v1);
    v2 = v1; v1 = v;
    snake_pnts(k,:) = snk.vert(k,:) + delta(v,:);
end

%% remesh snake
remesh_pnts = cell(1,n);
for k=1:n-1
    [X,Y,Z] = bresenham_line3d(snake_pnts(k,:), snake_pnts(k+1,:));
    remesh_pnts{k} = [X(1:end-1)', Y(1:end-1)', Z(1:end-1)'];
end
remesh_pnts{n} = snake_pnts(n,:);
tmp = vertcat(remesh_pnts{:});
snk.vert = tmp(1:min(10,size(tmp,1)),:);

end

