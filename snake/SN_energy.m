function Esnk = SN_energy( V, vert, overlap_arr, Eattact, ALPHA, BETA, GAMMA, ZETA )
%SN_ENERGY calculate snake energy

n = size(vert,1);

ds = diff(vert);
dslen = sqrt(sum(ds.^2,2));
newdbar = mean(dslen);
Econt = abs(dslen - newdbar);

dss = diff(ds);
Ecurv = sum(dss.^2,2);

idx = sub2ind(size(V), vert(:,2), vert(:,1), vert(:,3));
Eimg = V(idx);
Eimg(overlap_arr) = max(0,Eimg(overlap_arr));

Eatt = zeros(n,1);
Eatt(1) = Eattact(1);
Eatt(n) = Eattact(2);

alpha = ALPHA*ones(1,n);
beta = BETA*ones(1,n);
gamma = GAMMA*ones(1,n);
zeta = ZETA*ones(1,n);

Esnk = alpha(2:end)*Econt + beta(2:end-1)*Ecurv + gamma*Eimg + zeta*Eatt;

% if snk.num == 3
%     msg = sprintf('Snake %d => Econt = %f, Ecurv = %f, Eimg = %f, Esnk = %f', ...
%             snk.num, alpha(2:end)*Econt, beta(2:end-1)*Ecurv, gamma*Eimg, Esnk); 
%     disp(msg);
% end

end

