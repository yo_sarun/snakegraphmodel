function data = SN_postprocess( Iout, snakes, collide, scale, varargin )
%SN_POSTPROCESS Summary of this function goes here
%   Detailed explanation goes here

defaultoptions = struct('maxiter', 3, 'length_thr', 20);
options = get_options(defaultoptions, varargin);

BLOCKSIZE = 8;      % grid bin for finding adjacent points

s = size(scale);
blocks = cell(ceil(s./BLOCKSIZE));
gridsize = size(blocks);

%% keep only the largest snake component
collide = collide + collide';
CC = components(collide);
maxCC = mode(CC);
snakes = snakes(CC == maxCC);
n = length(snakes);

collide = collide(CC==maxCC, CC==maxCC);
figure(6), clf, SN_show(Iout, snakes, collide);

%% adjust snake point along GVF
mu = .2;
GVF_ITER = 10;
h = fspecial('gaussian', [3, 3], 3);
f = imfilter(Iout,h);
Fexts = AM_GVF(f, mu, GVF_ITER, 1);
Fext = {double(Fexts(:,:,:,1)), double(Fexts(:,:,:,2)), double(Fexts(:,:,:,3))};
% figure(10); AC_quiver( Fext{1}(:,:,17), Fext{2}(:,:,17), mean(Iout, 3));

for iter = 1:options.maxiter
    for i = 1:n
        maxsize = ones(size(snakes(i).vert,1), 1) * s([2 1 3]);
        vert = zeros(size(snakes(i).vert));
        for k = 1:3
            % interpolate the external force for vertices within the range
            vert(:,k) = snakes(i).vert(:,k) + 0.5 .* ba_interp3(Fext{k},...
                    snakes(i).vert(:,1), snakes(i).vert(:,2), snakes(i).vert(:,3)...
                    ,'linear');
        end
        vert(vert < 1) = 1;
        vert(vert > maxsize) = maxsize(vert > maxsize);
        snakes(i).vert = vert;
    end
end
figure(7), clf, SN_show(Iout, snakes, collide);
% keyboard;

%% build graph
scan = -1:1:1;
[delta_x, delta_y, delta_z] = meshgrid(scan, scan, scan);
delta = [delta_x(:), delta_y(:), delta_z(:)];
max_size = ones(27,1) * gridsize;

N = vertcat(snakes.vert);
radius = ba_interp3(scale, N(:,1), N(:,2), N(:,3), 'linear');
E = [];
snkcur = cumsum([0 ,arrayfun(@(x)size(x.vert,1), snakes)]);
collidel = tril(collide);
for i = 1:n
    % keep only the biggest tree
    cur = snkcur(i);
    ni = size(snakes(i).vert,1);
    % add edges along the snake
    E = [E; [cur+1:cur+ni-1; cur+2:cur+ni]'];
    % add edges from collide
    nbs = find(collidel(i,:));
    for nb = nbs
        dmin = inf;
        vert = [];
        for j = 1:ni
            ds = bsxfun(@minus, snakes(nb).vert, snakes(i).vert(j,:));
            dslen = sqrt(sum(ds.^2,2));
            % connect to closest one
            [dsmin, pos] = min(dslen);
            if dsmin < dmin
                % keep only the shortest edge
                dmin = dsmin;
                vert = [cur+j, snkcur(nb)+pos];
            elseif abs(dsmin - dmin) < 0.001
                % multiple edges with the shortest length
                vert = [vert; cur+j, snkcur(nb)+pos];
            end
        end
        if isempty(vert), keyboard; end
        if pos < 1, keyboard; end
        E = [E; vert];
    end
    % add edges between adjacent snake points (within its radius)
    % divide snake points into grid for speed
    snkcells = ceil(snakes(i).vert./BLOCKSIZE);
    
    for j = 1:ni
        % get index of neighboring blocks/cells
        nb_blk = (ones(27,1) * snkcells(j,:)) + delta;
        cond = all(nb_blk >= 1 & nb_blk <= max_size, 2);
        idx = nb_blk(cond,1) + (nb_blk(cond,2)-1)*gridsize(1) + (nb_blk(cond,3)-1)*gridsize(1)*gridsize(2);
        % check adjacent snake points in neighbour blocks
        nb_pnt = [blocks{idx}];
        if ~isempty(nb_pnt)
            dslen = sqrt(sum(bsxfun(@minus, N(nb_pnt,:), N(cur+j,:)).^2,2));
            pin = dslen <= radius(nb_pnt) | dslen <= radius(cur+j);
            node_to = nb_pnt(pin);
            E = [E; (cur+j) * ones(size(node_to')) ,node_to'];
        end
        
        % add snake point number into snkcells
        blocks{snkcells(j,2), snkcells(j,1), snkcells(j,3)} = ...
                [blocks{snkcells(j,2), snkcells(j,1), snkcells(j,3)}, cur+j];
    end
end

S = sqrt(sum(bsxfun(@minus, N(E(:,1),:), N(E(:,2),:)).^2,2))+0.001;
G = sparse(E(:,1), E(:,2), S, snkcur(end), snkcur(end));
G = G + G';
count = sparse(E(:,1), E(:,2), 1, snkcur(end), snkcur(end));
count = count + count';
G(count==2) = G(count==2) ./ 2;

figure(8), clf, SN_graph( G, N, Iout );

%% extract trace from graph
CC = components(G);
maxCC = mode(CC);
G = G(CC==maxCC,CC==maxCC);
N = N(CC==maxCC,:);

% find root
[~,root] = min(sum(bsxfun(@minus, N, mean(N)).^2,2));

% find path to all leaves
T = kruskal_mst(G);
[~, ~, pred] = bfs(T, root);
% [~, pred] = shortest_paths(G, root);

[n1,n2] = find(T);
degree = histc([n1;n2]',1:size(N,1)) / 2;

leaf = find(degree==1);
paths = cell(size(leaf));
pathlen = zeros(size(leaf));
for i = 1:length(leaf)
    path = path_from_pred(pred, leaf(i));
    paths{i} = path;
    pathlen(i) = sum(sqrt(sum((N(path(1:end-1),:) - N(path(2:end),:)).^2,2)));
end

% add one path at a time to trace tree (longest path first)
[~, B] = sort(pathlen, 'descend');
visited = false(size(N,1),1);
E = [];
for i = B
    % remove overlap
    path = paths{i};
    pos = find(visited(path), 1, 'last');
    if isempty(pos), 
        pos = 1; 
    end
    path = path(pos:end);
    if length(path) < options.length_thr || length(path) < 2, 
        continue
    end
    % add to tree
    visited(path) = true;
    E = [E; path(1:end-1)', path(2:end)'];
    
    % smooth
    % ** may need to adjust depth **
    for iter = 1:5
        % get radius, and weighted snake points
        radius = ba_interp3(scale, N(path,1), N(path,2), N(path,3), 'linear');
        radius(radius < 1) = 1;
        wvert = bsxfun(@rdivide, N(path,:), radius);
        % get weight sum and normalize term, then update snake points
        wsum = wvert(1:end-2,:)+wvert(2:end-1,:)+wvert(3:end,:);
        marginal = 1./radius(1:end-2)+1./radius(2:end-1)+1./radius(3:end);
        if any(any(isnan(bsxfun(@rdivide, wsum, marginal))))
            keyboard;
        end
        N(path(2:end-1),:) = bsxfun(@rdivide, wsum, marginal);
    end
end

G = sparse(E(:,1), E(:,2), 1, size(N,1), size(N,1));
keep = unique(E);
G = G(keep,keep);
G = G + G';
N = N(keep,:);

%% convert to swc format
n = size(N,1);
[order,pred] = bfs_traverse(G, 1);
data = [(1:n)', 2*ones(n,1), N, ones(n,2)];
data = data(order,:);
data(:,7) = pred(order);
% get fixed index
fixind = zeros(size(order));
fixind(order) = 1:length(order);
% change to fixed index
data(:,1) = fixind(data(:,1));
data(1,7) = -1;
data(2:end,7) = fixind(data(2:end,7));
% fix coordinate from [1,n] to [0,n-1]
data(:,3:5) = data(:,3:5)-1;

end

