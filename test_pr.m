clc

addpath(genpath('matlab_bgl'))
addpath('metric')
addpath('imagevolume')
addpath('utils');

goldswc = EV_readSWC('goldd.swc');
testswc = EV_readSWC('testd.swc');

figure(1), EV_plot( [], goldswc, '3D');
figure(2), EV_plot( [], testswc, '3D');

[score, m, e] = EV_diadem('metric/goldd.swc', 'metric/testd.swc')
[p,r,mi,ex] = EV_pr(goldswc, testswc)
