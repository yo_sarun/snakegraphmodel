close all
clear all
clc


addpath('AMT');
addpath('CSF');
addpath('frangi_filter_version2a');
addpath('PreprocessLib');
addpath('oof3response');
addpath('bresenham_line3d');
addpath(genpath('CurveLab-2.1.3'));
addpath(genpath('matlab_bgl'));
addpath('ba_interp3');
addpath('SteerTV')

addpath('HCF');
addpath('imagevolume');
addpath('snake');
addpath('metric');
addpath('utils');

% set(0,'DefaultFigureWindowStyle','normal')
set(0,'DefaultFigureWindowStyle','docked')

%% parameters
options = struct('oof_thr', 4, 'zeta', 0.5, 'preproc', 1, 'e_att', true);


DEBUG = 1;
SAVE_AVI = 0;

OOF_thr = options.oof_thr;
% OOF_thr = 4;        % threshold of OOF response
% OOF_thr = 3;        % 73113_2
ZRATIO = 4;         % z-ratio against xy of pixel size
sigmas = 2;         % var of guassian smooth for find neurite direction

max_iter = 500;     % max number of HCF iteration
BLOCKSIZE = 20;     % size of snake grid cell, for optimize compute model

ALPHA = 0.5;        % snake parameters
BETA = 0.1;
GAMMA = 2.0;
ZETA = options.zeta;
% ZETA = 0.5;         % region, 72513_h0
% ZETA = 0.3;         % 72413_h0, 73113_1
MAXV = -log(1/2);   % max value of Eimg
MAXCON = 3;         % no. of non-decrease Esnk allowed before converge

if exist('SCRIPT','var')
    DEBUG = 0;
else
%     imgdir = '../Neuron/72413/72413_h0/';
%     imgdir = '../Neuron/72513/72513_h0/';
%     imgdir = '../Neuron/73113_1/Late 1st - early 2nd instar/73113_1_L1E2_0/';
%     imgdir = '../Neuron/73113_2/CD8 timelapse - late 3rd instar/73113_2_L3_h0/';
    imgdir = '../ImageRegion/img72413_h0/';
    % imgdir = '../ImageRegion/img72513_h0/';
    savefile = 'swc/region/crfimg72413_h0.swc';
    % savefile = 'swc/ncrf73113_2_L3_h0.swc';
%     savefile = 'swc/crf73113_2_L3_h0.swc';
end

%% preprocess image
if 1 %exist('temp.mat','file') ~= 2
    disp(['Read Image : ' imgdir])
    V = IV_loadimg(imgdir);

    % preprocess image (may need to improve by adjust brightness locally)
    disp('Preprocess Volume Started...')
    [Vpre, Iout1, scale] = IV_preprocess(V, ZRATIO, ...
            'intensity_thr', 50, ...
            'region_size', 400);
    [Iout2, ~] = IV_tensor3D( Vpre );
%     Iout = IV_opticalflow( Vpre );
    [Iout3, ~] = IV_frangi( Vpre );
    Iout = mean(cat(4,Iout1, Iout2, Iout3),4);
%     save('temp.mat','V','Vpre','Iout','scale')
else
    load('temp.mat')
end

if DEBUG
%     Vpre = Vpre(651:750,201:300, :);
%     Iout = Iout(651:750,201:300, :);
%     scale = scale(651:750,201:300, :);

%     Vpre = Vpre(1:100, 1:100, :);
%     Iout = Iout(1:100, 1:100, :);
%     scale = scale(1:100, 1:100, :);

%     Vpre = Vpre(51:150,721:820, :);
%     Iout = Iout(51:150,721:820, :);
%     scale = scale(51:150,721:820, :);

%     Vpre = Vpre(1:200, 1:200, :);
%     Iout = Iout(1:200, 1:200, :);
%     scale = scale(1:200, 1:200, :);

%     Vpre = Vpre(1:300, 1:300, :);
%     Iout = Iout(1:300, 1:300, :);
%     scale = scale(1:300, 1:300, :);
    
%     Vpre = Vpre(1:200, 1:512, :);
%     Iout = Iout(1:200, 1:512, :);
%     scale = scale(1:200, 1:512, :);
    
%     Vpre = Vpre(:, 1:512, :);
%     Iout = Iout(:, 1:512, :);
%     scale = scale(:, 1:512, :);

%     Vpre = Vpre(851:950,901:1000,:);
%     Iout = Iout(851:950,901:1000, :);
%     scale = scale(851:950,901:1000, :);
end

% Get neurite direction
disp('Get Neurite Direction Started...')
[Vx, Vy, Vz] = IV_eig(Iout, sigmas);

% find seed points (need to improve by remove seed like in Fua paper)
disp('Find Seed Point Started...')
[Vseed, seedp] = IV_findseed( Iout, OOF_thr ); 
if DEBUG
    figure(1); imshow3D(V);
    figure(2); imshow3Doverlay(Vpre, Vseed);
    figure(11); imshow3D(Iout);
end

%% convert OOF response to energy function
% also give penalty when intensity is lower than threshold
E = -log((1+Iout)./2);

%% initialize snakes
disp('Initialize Snakes Started...')
snakes = SN_initsnake(E, seedp, Vx, Vy, Vz, ALPHA, BETA, GAMMA, ZETA);

%% local highest confidence first (Local HCF) inits
n = length(snakes);
collide = spalloc(n,n,4);
[stability, graph, snkgrid] = HCF_initstability(snakes, E, BLOCKSIZE);
[stability, graph, snkgrid] = HCF_updatestability(...
        snakes, stability, graph, snkgrid, true(1,n), BLOCKSIZE, MAXCON, ZRATIO);

%% debug
figure(3), SN_show(Vpre, snakes, [], scale);
if SAVE_AVI,
    outputVideo = VideoWriter('yoyo3.avi');
    outputVideo.FrameRate = 4;
    open(outputVideo);
    set(gca,'position',[0 0 1 1],'units','normalized')
    print(3, '-dpng', 'tmp.png', '-r300')
    img = imread('tmp.png');
    writeVideo(outputVideo,im2frame(img));
end
if DEBUG
    figure(10); AC_quiver( Vx(:,:,1), Vy(:,:,1), max(Vpre, [], 3));
end
% keyboard;
%% local highest confidence first (Local HCF) starts
disp('HCF Started...')
for iter = 1:max_iter
    disp(['Iter: ' num2str(iter) ', Total snake energy = ', num2str(sum([snakes(:).E]))]);
    
    % pre-compute neighborhood variables to allow parallel
    nbhd_rank = zeros(1,n);
    nbhd_snakes = cell(1,n);
    for i = 1:n
        idx = find(graph(i,:));
        [~, minpos] = min(stability(idx));
        nbhd_rank(i) = idx(minpos);
        nbhd_snakes{i} = snakes(idx(idx~=i));
    end
    
    % deform every snake
    change = false(1,n);
    for i = 1:n
        % evolve snake that has non-positive stability, lower stability
        % than its neighbour. If stability is equal, index (or rank) of
        % snake must be lower.
        if stability(i) < 0 && i == nbhd_rank(i)
            % evolve snake using dynamic programming
            % add interaction potential (include snk length in formula)
            % site merge when snk merge (so no interaction potential, 
            % only snk energy)
            if options.e_att
                [snakes(i), collide(i,:)] = SN_deform_att(...
                        snakes(i), nbhd_snakes{i}, E, collide(i,:), scale, ...
                        ALPHA, BETA, GAMMA, ZETA, MAXV, MAXCON, ZRATIO);
            else
                [snakes(i), collide(i,:)] = SN_deform(...
                        snakes(i), nbhd_snakes{i}, E, collide(i,:), scale, ...
                        ALPHA, BETA, GAMMA, ZETA, MAXV, MAXCON, ZRATIO);
            end
            change(i) = true;
        else
            snakes(i).minE = snakes(i).E;
        end
    end

    % check if no changes, population of snakes converge
    any_change = any(change);
    if ~any_change
        break
    end
    
    % compute snake components for attraction energy
    lenCC = -ones(1,n);
    comp = components(collide+collide');
    for i = 1:n
        if lenCC(comp(i)) < 0
            lenCC(comp(i)) = sum(arrayfun(@(x)size(x.vert,1), snakes(comp==comp(i))));
        end
        snakes(i).CClen = lenCC(comp(i));
        snakes(i).CCno = comp(i);
    end
    
    %** may need to mex this to speed up**
    [stability, graph, snkgrid] = HCF_updatestability(...
            snakes, stability, graph, snkgrid, change, BLOCKSIZE, MAXCON, ZRATIO);
%     pause
    % display result step-by-step
    figure(4), SN_show(Vpre, snakes, collide);
    if SAVE_AVI,
        set(gca,'position',[0 0 1 1],'units','normalized')
        print(4, '-dpng', 'tmp.png', '-r300')
        img = imread('tmp.png');
        writeVideo(outputVideo,im2frame(img));
    end
end
disp('Done...')

% %%
% figure(5), SN_show(Vpre, snakes, collide, scale);
% newsnakes = SN_smooth( snakes, scale );
% figure(6), SN_show(Iout, newsnakes, collide);
% 
% if SAVE_AVI,
%     for a = 5:6
%         figure(a);
%         set(gca,'position',[0 0 1 1],'units','normalized')
%         print(a, '-dpng', 'tmp.png', '-r300')
%         img = imread('tmp.png');
%         writeVideo(outputVideo,im2frame(img));
%     end
% end

%%
figure(5), SN_show(Vpre, snakes, collide, scale);
data = SN_postprocess( Iout, snakes, collide, scale );
% data = SN_post( snakes, collide+collide', scale );
figure(20); EV_plot( V, data, '2D' );
figure(21); EV_plot( [], data, '3D' );

print(20, '-dpng', [savefile '.png'], '-r300')

% write to file
if ~isempty(savefile)
    fileID = fopen(savefile, 'w');
    fprintf(fileID,'%d %d %.3f %.3f %.3f %.4f %d\n',data');
    fclose(fileID);
    disp(['Save SWC file: ' savefile ' completed...'])
end

if SAVE_AVI,
    figure(20);
    set(gca,'position',[0 0 1 1],'units','normalized')
    print(20, '-dpng', 'tmp.png', '-r300')
    img = imread('tmp.png');
    writeVideo(outputVideo,im2frame(img));
    close(outputVideo);
    delete('tmp.png');
end


%% %**********************************************************************
% newsnakes = SN_radius(Vpre, scale, snakes);
% data = SN_export(newsnakes, edges, 1, 'swc/ncrf72413_h0.swc');
%% %**********************************************************************
% % snakes = SN_radius(Vpre, scale, snakes);
% % disp('Done');
% % figure(20), SN_show(Vpre, snakes, collide, scale);
% 
% % %% convert snakes to graph
% % [newsnakes, edges] = SN_overlap(snakes, collide+collide', graph, scale);
% % figure(5), SN_plot(Vpre, newsnakes, edges);
% % figure(20), SN_show(Vpre, newsnakes);
% % if SAVE_AVI,
% %     set(gca,'position',[0 0 1 1],'units','normalized')
% %     print(5, '-dpng', 'tmp.png', '-r300')
% %     img = imread('tmp.png');
% %     writeVideo(outputVideo,im2frame(img));
% % end
% % 
% % %%
% % data = SN_export(newsnakes, edges, 1, 'swc/region/2crfimg72413_h0.swc');
% % figure(6); EV_plot( Vpre, data );
% % 
% % if SAVE_AVI,
% %     set(gca,'position',[0 0 1 1],'units','normalized')
% %     print(6, '-dpng', 'tmp.png', '-r300')
% %     img = imread('tmp.png');
% %     writeVideo(outputVideo,im2frame(img));
% %     close(outputVideo);
% %     delete('tmp.png');
% % end
% 
%% %********************************************************************
% data = EV_readSWC( 'swc/t72413_h0.swc' );
% figure(51); EV_plot( V, data, '2D' );
% 
% data = EV_readSWC( 'swc/region/yoyonimg72413_h0.swc' );
% figure(52); EV_plot( [], data, '3D' );
% 
% data = EV_readSWC( 'swc/region/roy72413_h0.swc' );
% figure(53); EV_plot( [], data, '3D' );